<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class siboyong extends Model
{
    protected $table = 'siboyong';
    protected $primaryKey = 'id_boyong';
    protected $fillable = ['nomor_surat', 'tanggal_boyong', 'status', 'id_santri', 'user_id'];
    public function datasantri()
    {
        return $this->belongsTo('App\datasantri', 'id_santri', 'id_santri');
    }
    public function User()
    {
        return $this->belongsTo('App\User',);
    }
}
