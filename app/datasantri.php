<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class datasantri extends Model
{
    //
    protected $table = 'data_santri';
    protected $primaryKey = 'id_santri';
    protected $fillable = ['nomor_induk', 'nama_santri', 'jenis_kelamin', 'Provinsi', 'Kota', 'Alamat', 'asrama', 'tingkat_pendidikan', 'tingkat_diniyah', 'tanggal_lahir', 'tempat_lahir', 'nama_wali', 'no_hp', 'foto_santri', 'tanggal_daftar', 'status'];
    public function sipergi()
    {
        return $this->hasMany('App\sipergi', 'id_santri', 'id_santri');
    }
    public function sipulang()
    {
        return $this->hasMany('App\sipulang', 'id_santri', 'id_santri');
    }
    public function siboyong()
    {
        return $this->hasMany('App\siboyong', 'id_santri', 'id_santri');
    }
    public function langgar()
    {
        return $this->hasMany('App\langgar', 'id_santri', 'id_santri');
    }
}
