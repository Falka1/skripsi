<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class langgar extends Model
{
    protected $table = 'langgar';
    protected $primaryKey = 'id_langgar';
    protected $fillable = ['tanggal_langgar', 'keterangan', 'id_santri', 'user_id'];
    public function datasantri()
    {
        return $this->belongsTo('App\datasantri', 'id_santri', 'id_santri');
    }
    public function User()
    {
        return $this->belongsTo('App\User',);
    }
}
