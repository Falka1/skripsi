<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sipulang extends Model
{
    protected $table = 'sipulang';
    protected $primaryKey = 'id_pulang';
    protected $fillable = ['nomor_surat', 'tujuan', 'keperluan', 'tanggal_pergi', 'tanggal_balik', 'status', 'id_santri', 'user_id'];
    public function datasantri()
    {
        return $this->belongsTo('App\datasantri', 'id_santri', 'id_santri');
    }
    public function User()
    {
        return $this->belongsTo('App\User',);
    }
}
