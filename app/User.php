<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role',
    ];
    public function sipergi()
    {
        return $this->hasMany('App\sipergi',);
    }
    public function sipulang()
    {
        return $this->hasMany('App\sipulang',);
    }
    public function siboyong()
    {
        return $this->hasMany('App\siboyong',);
    }
    public function langgar()
    {
        return $this->hasMany('App\langgar',);
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
