<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sipergi extends Model
{
    protected $table = 'sipergi';
    protected $primaryKey = 'id_pergi';
    protected $fillable = ['tujuan', 'keterangan', 'waktu_pergi', 'waktu_balik', 'status', 'id_santri', 'user_id'];
    public function datasantri()
    {
        return $this->belongsTo('App\datasantri', 'id_santri', 'id_santri');
    }
    public function User()
    {
        return $this->belongsTo('App\User',);
    }
}
