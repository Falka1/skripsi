<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class LanggarController extends Controller
{
    public function index()
    {
        $langgar = \App\langgar::orderBy('created_at', 'desc')->get();
        $datasantri = \App\datasantri::all();
        return view('langgar.index', ['langgar' => $langgar, 'datasantri' => $datasantri]);
    }
    public function store(Request $request)
    {
        // dd($request->all());
        // return $request;
        $request->validate([
            'id_santri' => 'required',
            'keterangan' => 'required',
            'tanggal_langgar' => 'required|date',
            'user_id' => 'required',
        ]);

        \App\langgar::create([
            'keterangan' => $request->keterangan,
            'tanggal_langgar' => $request->tanggal_langgar,
            'id_santri' => $request->id_santri,
            'user_id' => $request->user_id,
        ]);

        return redirect('/langgar')->with('status', 'Surat Pelanggaran Berhasil Ditambahkan!');
    }
    public function destroy($id)
    {
        $langgar = \App\langgar::find($id);
        $langgar->delete($langgar);

        return redirect('/langgar')->with('status', 'Surat berhasil dihapus');
    }
    public function edit($id)
    {
        $langgar = \App\langgar::find($id);
        $datasantri = \App\datasantri::all();
        // dd($langgar);
        return view('langgar.edit', ['langgar' => $langgar, 'datasantri' => $datasantri]);
    }
    public function update(Request $request, $id)
    {
        $langgar = \App\langgar::find($id);
        // dd($langgar);
        $langgar->update([
            'keterangan' => $request->keterangan,
            'tanggal_langgar' => $request->tanggal_langgar,
            'id_santri' => $request->id_santri,
            'user_id' => $request->user_id,
        ]);


        return redirect('/langgar')->with('status', 'Surat Berhasil Diubah');
    }
    public function exportpdf($id)
    {
        $langgar = \App\langgar::find($id);
        $pdf = PDF::loadView('langgar.cetak', ['langgar' => $langgar])->setPaper('a5', 'potrait');
        return $pdf->stream('SuratBoyong.pdf');
    }
}
