<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AdminController extends Controller
{
    public function index()
    {
        $admin = \App\User::all();
        return view('admin.index', ['admin' => $admin]);
    }
    public function create()
    {
        return view('admin.create');
    }
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'email' => 'required|unique:data_santri',
            'name' => 'required',
            'role' => 'required',
            'password' => 'required|min:8',
        ]);

        $admin = new \App\User;
        $admin->name = $request->name;
        $admin->role = $request->role;
        $admin->email = $request->email;
        $admin->password = bcrypt($request->password);
        $admin->remember_token = Str::random(60);
        $admin->save();

        return redirect('/admin')->with('status', 'Data Santri Berhasil Ditambahkan!');
    }
    public function destroy($id)
    {
        $admin = \App\User::find($id);
        $admin->delete($admin);

        return redirect('/admin')->with('status', 'data berhasil dihapus');
    }
    public function edit($id)
    {
        $admin = \App\User::find($id);
        return view('admin.edit', ['admin' => $admin]);
    }
    public function update(Request $request, $id)
    {
        $admin = \App\User::find($id);
        $admin->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role' => $request->role,
        ]);


        return redirect('/admin')->with('status', 'Data Berhasil Diubah');
    }
}
