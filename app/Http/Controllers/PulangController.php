<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class PulangController extends Controller
{
    public function index()
    {
        $pulang = \App\sipulang::orderBy('created_at', 'desc')->get();
        $datasantri = \App\datasantri::all();
        return view('sipulang.index', ['pulang' => $pulang, 'datasantri' => $datasantri]);
    }
    public function store(Request $request)
    {
        // dd($request->all());
        // return $request;
        $request->validate([
            'id_santri' => 'required',
            'nomor_surat' => 'required',
            'tujuan' => 'required',
            'keperluan' => 'required',
            'tanggal_pergi' => 'required|date',
            'tanggal_balik' => 'required|date',
            'status' => 'sometimes|boolean',
            'user_id' => 'required',
        ]);

        \App\sipulang::create([
            'tujuan' => $request->tujuan,
            'nomor_surat' => $request->nomor_surat,
            'keperluan' => $request->keperluan,
            'tanggal_pergi' => $request->tanggal_pergi,
            'tanggal_balik' => $request->tanggal_balik,
            'id_santri' => $request->id_santri,
            'user_id' => $request->user_id,
            'status' => $request->status,
        ]);

        return redirect('/pulang')->with('status', 'Surat Izin Pulang Berhasil Ditambahkan!');
    }
    public function edit($id)
    {
        $pulang = \App\sipulang::find($id);
        $datasantri = \App\datasantri::all();
        // dd($pulang);
        return view('sipulang.edit', ['pulang' => $pulang, 'datasantri' => $datasantri]);
    }
    public function update(Request $request, $id)
    {
        $pulang = \App\sipulang::find($id);
        // dd($pulang);
        $pulang->update([
            'tujuan' => $request->tujuan,
            'nomor_surat' => $request->nomor_surat,
            'keperluan' => $request->keperluan,
            'tanggal_pergi' => $request->tanggal_pergi,
            'tanggal_balik' => $request->tanggal_balik,
            'id_santri' => $request->id_santri,
            'user_id' => $request->user_id,
            'status' => $request->status,
        ]);


        return redirect('/pulang')->with('status', 'Surat Berhasil Diubah');
    }
    public function destroy($id)
    {
        $pulang = \App\sipulang::find($id);
        $pulang->delete($pulang);

        return redirect('/pulang')->with('status', 'Surat berhasil dihapus');
    }
    public function exportpdf($id)
    {
        $pulang = \App\sipulang::find($id);
        $pdf = PDF::loadView('sipulang.cetak', ['pulang' => $pulang])->setPaper('a4', 'potrait');
        return $pdf->stream('SIPulang.pdf');
    }
}
