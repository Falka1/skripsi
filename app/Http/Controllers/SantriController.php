<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\datasantri;
use SebastianBergmann\Environment\Console;

class SantriController extends Controller
{
    public function index()
    {
        $datasantri = datasantri::all();
        return view('santri.table', ['datasantri' => $datasantri]);
    }
    public function create()
    {
        return view('santri.create');
    }



    public function store(Request $request)
    {
        // dd($request->all()); 
        //return $request;
        $request->validate([
            'nomor_induk' => 'required|unique:data_santri',
            'nama_santri' => 'required',
            'jenis_kelamin' => 'required|boolean',
            'Provinsi' => 'required',
            'Kota' => 'required',
            'Alamat' => 'required',
            'asrama' => 'required',
            'tingkat_pendidikan' => 'required',
            'tingkat_diniyah' => 'required',
            'tanggal_lahir' => 'required|date',
            'tempat_lahir' => 'required',
            'nama_wali' => 'required',
            'no_hp' => 'required|max:12',
            'foto_santri' => 'sometimes|image|mimes:jpg,jpeg,bmp,svg,png|max:5000',
            'tanggal_daftar' => 'required|date',
            'status' => 'sometimes|boolean',
        ]);

        $imgName = 'default.png';

        if ($request->hasFile('foto_santri')) {
            $imgName = $request->file('foto_santri')->getClientOriginalName() . '-' . time() . '.' . $request->file('foto_santri')->extension();
            $request->file('foto_santri')->move(public_path('images'), $imgName);
        }
        if ($request->status == null) {
            $request->status = '0';
        }

        datasantri::create([
            'nomor_induk' => $request->nomor_induk,
            'nama_santri' => $request->nama_santri,
            'jenis_kelamin' => $request->jenis_kelamin,
            'Provinsi' => $request->Provinsi,
            'Kota' => $request->Kota,
            'Alamat' => $request->Alamat,
            'asrama' => $request->asrama,
            'tingkat_pendidikan' => $request->tingkat_pendidikan,
            'tingkat_diniyah' => $request->tingkat_diniyah,
            'tanggal_lahir' => $request->tanggal_lahir,
            'tempat_lahir' => $request->tempat_lahir,
            'nama_wali' => $request->nama_wali,
            'no_hp' => $request->no_hp,
            'foto_santri' => $imgName,
            'tanggal_daftar' => $request->tanggal_daftar,
            'status' => $request->status,
        ]);

        return redirect('/santri')->with('status', 'Data Santri Berhasil Ditambahkan!');
    }

    public function show($id)
    {
        $datasantri = datasantri::find($id);
        // dd($datasantri->langgar);
        return view('santri.show', ['datasantri' => $datasantri,],);
    }
    public function destroy($id)
    {
        $datasantri = datasantri::find($id);
        $datasantri->delete($datasantri);

        return redirect('/santri')->with('status', 'data berhasil dihapus');
    }
    public function edit($id)
    {
        $datasantri = datasantri::find($id);
        // dd($datasantri);
        return view('santri.edit', ['datasantri' => $datasantri]);
    }
    public function update(Request $request, $id)
    {
        $datasantri = datasantri::find($id);
        $datasantri->update($request->all());
        if ($request->hasFile('foto_santri')) {
            $imgName = $request->file('foto_santri')->getClientOriginalName() . '-' . time() . '.' . $request->file('foto_santri')->extension();
            $request->file('foto_santri')->move(public_path('images'), $imgName);
            $datasantri->foto_santri = $imgName;
            $datasantri->save();
        }

        return redirect('/santri')->with('status', 'Data Berhasil Diubah');
    }
}
