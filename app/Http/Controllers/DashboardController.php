<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class DashboardController extends Controller
{
    public function index()
    {
        $pergi = \App\sipergi::where('status', 0)->get();
        $pergi1 = DB::table('sipergi')
            ->select(DB::raw('DATE(created_at) as Tanggal'), DB::raw('count(*) as jumlah'))
            ->groupBy('tanggal')
            ->get();
        $langgar = DB::table('langgar')
            ->select(DB::raw("DATE_FORMAT(created_at, '%m-%Y') Bulan"),  DB::raw('count(*) as jumlah'),)
            ->groupby('Bulan')
            ->get();

        // dd($langgar);
        return view('items.index', ['pergi' => $pergi, 'pergi1' => $pergi1, 'langgar' => $langgar,]);
    }
    public function contact()
    {

        return view('items.contact');
    }
}
