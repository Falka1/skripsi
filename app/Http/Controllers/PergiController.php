<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class PergiController extends Controller
{
    public function index()
    {
        $pergi = \App\sipergi::orderBy('created_at', 'desc')->get();
        $datasantri = \App\datasantri::all();
        return view('sipergi.index', ['pergi' => $pergi, 'datasantri' => $datasantri]);
    }
    public function store(Request $request)
    {
        // dd($request->all());
        // return $request;
        $request->validate([
            'id_santri' => 'required',
            'tujuan' => 'required',
            'keterangan' => 'required',
            'waktu_pergi' => 'required',
            'waktu_balik' => 'required',
            'status' => 'sometimes|boolean',
            'user_id' => 'required',
        ]);

        \App\sipergi::create([
            'tujuan' => $request->tujuan,
            'keterangan' => $request->keterangan,
            'waktu_pergi' => $request->waktu_pergi,
            'waktu_balik' => $request->waktu_balik,
            'id_santri' => $request->id_santri,
            'user_id' => $request->user_id,
            'status' => $request->status,
        ]);

        return redirect('/pergi')->with('status', 'Surat Izin Pergi Berhasil Ditambahkan!');
    }
    public function edit($id)
    {
        $pergi = \App\sipergi::find($id);
        $datasantri = \App\datasantri::all();
        // dd($pergi);
        return view('sipergi.edit', ['pergi' => $pergi, 'datasantri' => $datasantri]);
    }
    public function update(Request $request, $id)
    {
        $pergi = \App\sipergi::find($id);
        // dd($pergi);
        $pergi->update([
            'tujuan' => $request->tujuan,
            'keterangan' => $request->keterangan,
            'waktu_pergi' => $request->waktu_pergi,
            'waktu_balik' => $request->waktu_balik,
            'id_santri' => $request->id_santri,
            'user_id' => $request->user_id,
            'status' => $request->status,
        ]);


        return redirect('/pergi')->with('status', 'Surat Berhasil Diubah');
    }
    public function destroy($id)
    {
        $pergi = \App\sipergi::find($id);
        $pergi->delete($pergi);

        return redirect('/pergi')->with('status', 'Surat berhasil dihapus');
    }

    public function exportpdf($id)
    {
        $pergi = \App\sipergi::find($id);
        $pdf = PDF::loadView('sipergi.cetak', ['pergi' => $pergi])->setPaper('a6', 'potrait');
        return $pdf->stream('SIPergi.pdf');
    }
}
