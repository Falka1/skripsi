<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class BoyongController extends Controller
{
    public function index()
    {
        $boyong = \App\siboyong::orderBy('created_at', 'desc')->get();
        $datasantri = \App\datasantri::all();
        return view('siboyong.index', ['boyong' => $boyong, 'datasantri' => $datasantri]);
    }
    public function store(Request $request)
    {
        // dd($request->all());
        // return $request;
        $request->validate([
            'id_santri' => 'required|unique:siboyong',
            'nomor_surat' => 'required|unique:siboyong',
            'tanggal_boyong' => 'required|date',
            'status' => 'sometimes|boolean',
            'user_id' => 'required',
        ]);

        \App\siboyong::create([
            'nomor_surat' => $request->nomor_surat,
            'tanggal_boyong' => $request->tanggal_boyong,
            'id_santri' => $request->id_santri,
            'user_id' => $request->user_id,
            'status' => $request->status,
        ]);

        return redirect('/boyong')->with('status', 'Surat Boyong Berhasil Ditambahkan!');
    }
    public function destroy($id)
    {
        $boyong = \App\siboyong::find($id);
        $boyong->delete($boyong);

        return redirect('/boyong')->with('status', 'Surat berhasil dihapus');
    }
    public function edit($id)
    {
        $boyong = \App\siboyong::find($id);
        $datasantri = \App\datasantri::all();
        // dd($boyong);
        return view('siboyong.edit', ['boyong' => $boyong, 'datasantri' => $datasantri]);
    }
    public function update(Request $request, $id)
    {
        $boyong = \App\siboyong::find($id);
        // dd($boyong);
        $boyong->update([
            'nomor_surat' => $request->nomor_surat,
            'tanggal_boyong' => $request->tanggal_boyong,
            'id_santri' => $request->id_santri,
            'user_id' => $request->user_id,
            'status' => $request->status,
        ]);


        return redirect('/boyong')->with('status', 'Surat Berhasil Diubah');
    }
    public function exportpdf($id)
    {
        $boyong = \App\siboyong::find($id);
        $pdf = PDF::loadView('siboyong.cetak', ['boyong' => $boyong])->setPaper('legal', 'potrait');
        return $pdf->stream('SuratBoyong.pdf');
    }
}
