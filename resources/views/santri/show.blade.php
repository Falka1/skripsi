@extends('adminlte.master')

@section('content')
<div class="card card-olive">
    <div class="card-header">
        <h3 class="card-title">Biodata Santri</h3>
    </div>
    <div class="card-body row">
        <div class="col-md-4">
            <div class="card">
              <div class="card-body  ">
                <div class="d-flex flex-column align-items-center text-center">
                  <img src="/images/{{$datasantri->foto_santri}} " alt="Admin" 
                    class="rounded-circle" style="width: 200px; height: 200px; background-position: center center; background-repeat: no-repeat;">
                  <div class="mt-3">
                    <h4>{{$datasantri->nama_santri}}</h4>
                    @if ($datasantri->status == 1)    
                        <h5><span class="badge badge-success">Aktif</span></h5>
                      @else 
                        <h5><span class="badge badge-danger">Tidak Aktif</span></h5>
                      @endif
                    <p class="text-muted font-size-sm text-capitalize mb-1">{{$datasantri->Kota}}, {{$datasantri->Provinsi}}</p>
                    <p class="text-secondary ">{{$datasantri->asrama}}</p>
                    <p class="text-secondary ">{{$datasantri->nomor_induk}}</p>
                  <div class="row mx-4">
                    <a type="button" class="btn btn-warning mx-2" href="/santri/{{$datasantri->id_santri }}/edit">Ubah</a>
                    @if(auth()->user()->role == 'superadmin')
                    <form action="/santri/{{$datasantri->id_santri }}" method="post">
                      @csrf
                      @method('DELETE')
                      <button type="submit" class="btn btn-danger mx-2" value='delete' onclick="return confirm('data akan dihapus...')">Hapus</button>
                    </form>
                    @endif
                  </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
            <div class="col-md-8">
                <div class="card mb-3">
                  <div class="card-body">
                    <div class="row">

                      <div class="col-sm-3">
                        <h6 class="mb-0">Jenis Kelamin</h6>
                      </div>
                      @if ($datasantri->jenis_kelamin == 1)
                        <div class="col-sm-9 text-secondary">
                          Laki-Laki
                        </div>
                      @else 
                        <div class="col-sm-9 text-secondary">
                          Perempuan
                        </div>
                      @endif
                      
                    </div>
                    <hr>
                    <div class="row">
                      <div class="col-sm-3">
                        <h6 class="mb-0">TTL</h6>
                      </div>
                      <div class="col-sm-9 text-secondary text-capitalize">
                        {{$datasantri->tempat_lahir}}, {{$datasantri->tanggal_lahir}}
                      </div>
                    </div>
                    <hr>
                    <div class="row">
                      <div class="col-sm-3">
                        <h6 class="mb-0">Pendidikan</h6>
                      </div>
                      <div class="col-sm-9 text-secondary ">
                        {{$datasantri->tingkat_pendidikan}} (Formal) {{$datasantri->tingkat_diniyah}} (diniyah)
                      </div>
                    </div>
                    <hr>
                    <div class="row">
                      <div class="col-sm-3">
                        <h6 class="mb-0">Nama Wali</h6>
                      </div>
                      <div class="col-sm-9 text-secondary text-capitalize">
                        {{$datasantri->nama_wali}}
                      </div>
                    </div>
                    <hr>
                    <div class="row">
                      <div class="col-sm-3">
                        <h6 class="mb-0">No. Telepon</h6>
                      </div>
                      <div class="col-sm-9 text-secondary text-capitalize">
                        {{$datasantri->no_hp}}
                      </div>
                    </div>
                    <hr>
                    <div class="row">
                      <div class="col-sm-3">
                        <h6 class="mb-0">Alamat Lengkap</h6>
                      </div>
                      <div class="col-sm-9 text-secondary text-capitalize">
                        {{$datasantri->Alamat}}
                      </div>
                    </div>
                    <hr>
                    <div class="row">
                      <div class="col-sm-3">
                        <h6 class="mb-0">Tanggal Pendaftaran</h6>
                      </div>
                      <div class="col-sm-9 text-secondary">
                        {{$datasantri->tanggal_daftar}}
                      </div>
                    </div>
                    <hr>
                  </div>
                </div>
        </div>
        <div class="col-md-12">
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Pelanggaran Santri</h3>
        </div>
          <div class="card-body">
              <table id="datasantri" class="table table-hover table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Santri</th>
                  <th>Tanggal Kejadian</th>
                  <th>Keterangan</th>
                  <th>Petugas</th>
                  <th >Action</th>
                </tr>
                </thead>
                <tbody>
                  @forelse($datasantri->langgar as $lgr)
                  <tr>
                    <th>{{$loop->iteration}}</th>
                    <td><a  href="/santri/{{$lgr->id_santri}}/show"> {{$lgr->datasantri->nama_santri}}</a></td>
                    <td>{{$lgr->tanggal_langgar}}</td>
                    <td>{{$lgr->keterangan}}</td>
                    <td> {{$lgr->User->name}}</td>
                    <td class='row'>
                      <a type="button" class="btn btn-success" href="/langgar/{{$lgr->id_langgar}}/pdf" target="_blank"><i class="fas fa-print"></i></a>
                      <a type="button" class="btn btn-warning" href="/langgar/{{$lgr->id_langgar}}/edit"><i class="fas fa-edit"></i></a>
                      @if(auth()->user()->role == 'superadmin')
                      <form action="/langgar/{{$lgr->id_langgar}}" method="post">
                        @csrf
                        @method('DELETE')
                      <button type="submit" class="btn btn-danger" value='delete' onclick="return confirm('data akan dihapus...')"><i class="far fa-trash-alt"></i></button>
                      </form>
                      @endif
                    </td>
                  </tr>
                  @empty
                    <tr>
                      <td colspan="6" align="center">data masih kosong</td>
                    </tr>
                  @endforelse
                </tbody>
              </table>
          </div>
          
        </div>
      </div>
    </div>
    <div class="card-footer">
        <a type="button" class="btn btn-primary" href="/santri">Kembali</a>
    </div>
</div>



@endsection