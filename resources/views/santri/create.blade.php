@extends('adminlte.master')

@section('content')

<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Pendaftaran Santri Baru</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action='/santri' method='POST' enctype="multipart/form-data">
              @csrf
                <div class="card-body">
                  <!-- input nomor induk -->
                  <div class="form-group">
                    <label for="nomor_induk">Nomor Induk</label>
                    <input type="text" class="form-control @error('nomor_induk') is-invalid @enderror" id="nomor_induk" name='nomor_induk' value="{{old('nomor_induk','')}} " placeholder="Masukkan nomor_induk">
                    @error('nomor_induk')<div class="invalid-feedback">{{$message}}</div>@enderror
                  </div>
                  <!-- input nomor nama -->
                  <div class="form-group">
                    <label for="nama_santri">Nama Santri</label>
                    <input type="text" class="form-control @error('nama_santri') is-invalid @enderror" id="nama_santri" name='nama_santri' value="{{old('nama_santri','')}} " placeholder="Masukkan Judul">
                    @error('nama_santri')<div class="invalid-feedback">{{$message}}</div>@enderror
                  </div>
                  <!--input nomor jenis kelamin-->
                  <fieldset class="form-group">
                    <div class="row">
                      <legend class="col-form-label col-sm-2 pt-0 ">Jenis Kelamin</legend>
                      <div class="col-sm-10">
                        <div class="form-check">
                          <input class="form-check-input " type="radio" name="jenis_kelamin" id="jk_laki" value="1">
                          <label class="form-check-label" for="jk_laki">
                            Laki-Laki
                          </label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="jenis_kelamin" id="jk_perempuan" value="0">
                          <label class="form-check-label" for="jk_perempuan">
                            Perempuan
                          </label>
                        </div>
                      </div>
                    </div>
                  </fieldset>
                  <!-- asal domisili-->
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="Provinsi">Pilih Provinsi</label>
                        <select class="form-control" id="Provinsi" name='Provinsi'>
                            <option value="Jawa Timur">JAWA TIMUR</option>
                            <option value="Aceh"> ACEH </option>
                            <option value="Sumatera Utara">SUMATERA UTARA</option>
                            <option value="Sumatra Barat">SUMATERA BARAT</option>
                            <option value="Riau"> KEPULAUAN RIAU</option>
                            <option value="Jambi">JAMBI</option>
                            <option value="Sumatera Selatan">SUMATERA SELATAN</option>
                            <option value="Bengkulu">BENGKULU</option>
                            <option value="Lampung">LAMPUNG</option>
                            <option value="Bangka Belitung">KEPULAUAN BANGKA BELITUNG</option>
                            <option value="Jakarta">DKI JAKARTA</option>
                            <option value="Jawa Barat">JAWA BARAT</option>
                            <option value="Jawa Tengah">JAWA TENGAH</option>
                            <option value="Yogyakarta">DI YOGYAKARTA</option>
                            <option value="Banten">BANTEN</option>
                            <option value="Bali">BALI</option>
                            <option value="Nusa Tenggara Barat">NUSA TENGGARA BARAT</option>
                            <option value="Nusa Tenggara Timur">NUSA TENGGARA TIMUR</option>
                            <option value="Kalimantan Barat">KALIMANTAN BARAT</option>
                            <option value="Kalimantan Tengah">KALIMANTAN TENGAH</option>
                            <option value="Kalimantan Selatan">KALIMANTAN SELATAN</option>
                            <option value="Kalimantan Timur">KALIMANTAN TIMUR</option>
                            <option value="Kalimantan Utara">KALIMANTAN UTARA</option>
                            <option value="Sulawesi Utara">SULAWESI UTARA</option>
                            <option value="Sulawesi Selatan">SULAWESI SELATAN</option>
                            <option value="Sulawesi Tengah">SULAWESI TENGAH</option>
                            <option value="Sulawesi Tenggara">SULAWESI TENGGARA</option>
                            <option value="Gorontalo">GORONTALO</option>
                            <option value="Sulawesi Barat">SULAWESI BARAT </option>
                            <option value="Maluku">MALUKU</option>
                            <option value="Maluku Utara">MALUKU UTARA</option>
                            <option value="Papua">PAPUA</option>
                            <option value="Papua Barat">PAPUA BARAT</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label for="Kota">Kota</label>
                      <input type="text" class="form-control @error('Kota') is-invalid @enderror" id="Kota" name='Kota' value="{{old('Kota','')}} " placeholder="tempat">
                      @error('Kota')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                  </div>
                  <!--alamat-->
                  <div class="form-group">
                      <label for="Alamat">Alamat Lengkap</label>
                      <textarea class="form-control @error('Alamat') is-invalid @enderror" id="Alamat" name="Alamat" rows="2"  placeholder="Enter ...">{{old('Alamat','')}}</textarea>
                      @error('Alamat')<div class="invalid-feedback">{{$message}}</div>@enderror
                  </div>
                  <!--input Asrama-->
                  <div class="form-group">
                    <label for="asrama">Pilih Asrama</label>
                    <select class="form-control" id="asrama" name='asrama'>
                      <option value='Al-jadid'>Al-Jadid</option>
                      <option value='Al-Hikam'>Al-Hikam</option>
                      <option value='Al-Taufiq'>Al-Taufiq</option>
                      <option value='Al-Huda'>Al-Huda</option>
                      <option value='Al-Tarbiyah'>At-Tarbiyah</option>
                      <option value="As-Syafaah">As-Syafa'ah</option>
                      <option value='An-Nadroh'>An-Nadroh</option>
                      <option value='Al-Karomah'>Al-Karomah</option>
                      <option value='Al-Hidayah'>Al-Hidayah</option>
                      <option value="Al-Ma'rifah">Al-Ma'rifah</option>
                      <option value='Istiqomah'>Istiqomah</option>
                    </select>
                  </div>
                  <!--input sekolah-->
                  <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="pendidikan">Pilih Pendidikan</label>
                    <select class="form-control" id="pendidikan" name='tingkat_pendidikan'>
                      <option value="SD">SD</option>
                      <option value="SMP">SMP</option>
                      <option value="SMA">SMA</option>
                      <option value="Universitas">Universitas</option>
                      <option value="Hanya Pondok">Hanya Pondok</option>
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="pendidikan">Pilih Pendidikan Diniyah</label>
                    <select class="form-control" id="pendidikan" name='tingkat_diniyah'>
                      <option value="MI">MI</option>
                      <option value="MTs">MTs</option>
                      <option value="MA">MA</option>
                    </select>
                  </div>
                  </div>
                  <!--lahiran-->
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="tanggal_lahir">Tanggal Lahir</label>
                      <input type="date" class="form-control" id="tanggal_lahir" name='tanggal_lahir' value={{old('tanggal_lahir','')}} >
                    </div>
                    <div class="form-group col-md-6">
                      <label for="tempat_lahir">Tempat Lahir</label>
                      <input type="text" class="form-control @error('tempat_lahir') is-invalid @enderror" id="tempat_lahir" name='tempat_lahir' value="{{old('tempat_lahir','')}} "placeholder="tempat lahir">
                      @error('tempat_lahir')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                  </div>
                  <!--nama wali-->
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="nama_wali">Nama Wali</label>
                      <input type="text" class="form-control @error('nama_wali') is-invalid @enderror" id="nama_wali" value='{{old('nama_wali','')}} ' name='nama_wali' >
                      @error('nama_wali')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                    <div class="form-group col-md-6">
                      <label for="no_hp">No. Hp</label>
                      <input type="number" class="form-control @error('no_hp') is-invalid @enderror" id="no_hp" name='no_hp' value="{{old('no_hp','')}} " placeholder="isi nomor wali">
                      @error('no_hp')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                  </div>
                  <!--file input-->
                  <div class="form-group">
                    <label for="exampleFormControlFile1">Upload Foto</label>
                    <input type="file" name='foto_santri' class="form-control-file" id="exampleFormControlFile1">
                  </div>
                  {{-- input tgl daftar --}}
                  <div class="form-group col-md-6">
                    <label for="tanggal_daftar">Tanggal Pendaftaran</label>
                    <input type="date" class="form-control" id="tanggal_daftar" name='tanggal_daftar' value={{old('tanggal_daftar','')}}  >
                  </div>

                  <!--input nomor jenis kelamin-->
                  @if(auth()->user()->role == 'superadmin')
                  <fieldset class="form-group">
                    <div class="row">
                      <legend class="col-form-label col-sm-2 pt-0">Status Santri</legend>
                      <div class="col-sm-10">
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="status" id="status_aktif" value="1" >
                          <label class="form-check-label" for="status_aktif">
                            Aktif
                          </label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="status" id="status_tidak" value="0" checked>
                          <label class="form-check-label" for="status_tidak">
                            Tidak Aktif
                          </label>
                        </div>
                      </div>
                    </div>
                  </fieldset>
                  @endif
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Simpan</button>
                  <a type="button" class="btn btn-primary" href="/santri">Kembali</a>
                </div>
              </form>
            </div>
@endsection
