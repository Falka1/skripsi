@extends('adminlte.master')
@section('content')
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Santri</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item active">Data Santri</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->

      <div class="card card-outline card-olive">
      <div class="card-header">
        <h3 class="card-title">TABEL DATA SANTRI</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        
        @if (session('status'))
          <div class="alert alert-success">
              {{session('status')}}
          </div>
        @endif

        <a class="btn btn-primary mb-2" href="/santri/create">Tambah Santri Baru</a>
        <div class="table-responsive">
        <table id="datasantri" class="table table-hover table-bordered table-striped">
          <thead>
          <tr>
            <th>No</th>
            <th>Nama Santri</th>
            <th>Asrama</th>
            <th>Pendidikan</th>
            <th>Asal</th>
            <th>Status</th>
            <th style="width:18%">Action</th>
          </tr>
          </thead>
          <tbody>
            @forelse($datasantri as $dts)
            <tr>
              <th>{{$loop->iteration}}</th>
              <td>{{$dts->nama_santri}}</td>
              <td>{{$dts->asrama}}</td>
              <td> {{$dts->tingkat_pendidikan}}</td>
              <td>{{$dts->Provinsi}}</td>
              @if ($dts->status == 1)
                  <td><span class="badge badge-success">Aktif</span></td>
              @else 
                <td><span class="badge badge-danger">Tidak Aktif</span></td>
              @endif
              <td class='row'>
                <a type="button" class="btn btn-primary" href="/santri/{{$dts->id_santri }}/show"><i class="far fa-eye"></i></a>
                <a type="button" class="btn btn-success" href="/santri/{{$dts->id_santri }}/edit"><i class="fas fa-edit"></i></a>
                @if(auth()->user()->role == 'superadmin')
                <form action="/santri/{{$dts->id_santri }}" method="post">
                  @csrf
                  @method('DELETE')
                <button type="submit" class="btn btn-danger" value='delete' onclick="return confirm('data akan dihapus...')"><i class="far fa-trash-alt"></i></button>
                </form>
                @endif
              </td>
            </tr>
            @empty
              <tr>
                <td colspan="7" align="center">data masih kosong</td>
              </tr>
            @endforelse
          </tbody>
          <tfoot>
          <tr>
            <th>No</th>
            <th>Nama Santri</th>
            <th>Asrama</th>
            <th>Pendidikan</th>
            <th>Asal</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
          </tfoot>
        </table>
        </div>
      </div>
      <!-- /.card-body -->
    </div>
      <!-- /.card -->
      

    </section>
    
@endsection

@push('script')
<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/jszip/jszip.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>
<script>
  $(function () {
    $("#datasantri").DataTable({
  "responsive": true, "lengthChange": false, "autoWidth": false,
  "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
}).buttons().container().appendTo('#datasantri_wrapper .col-md-6:eq(0)');
  });
  
</script>
@endpush

