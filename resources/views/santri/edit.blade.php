@extends('adminlte.master')

@section('content')

<div class="card card-warning">
              <div class="card-header">
                <h3 class="card-title">Ubah Data Santri</h3>
              </div>
              <!-- /.card-header --> 
              <!-- form start -->
              <form role="form" action='/santri/{{$datasantri->id_santri}}/update' method='POST' enctype="multipart/form-data">
                @method('PATCH')
                @csrf
                <div class="card-body">
                  <!-- input nomor induk -->
                  <div class="form-group">
                    <label for="nomor_induk">Nomor Induk</label>
                    <input type="text" class="form-control @error('nomor_induk') is-invalid @enderror" id="nomor_induk" name='nomor_induk' value="{{$datasantri->nomor_induk}}" placeholder="Masukkan nomor_induk">
                    @error('nomor_induk')<div class="invalid-feedback">{{$message}}</div>@enderror
                  </div>
                  <!-- input nomor nama -->
                  <div class="form-group">
                    <label for="nama_santri">Nama Santri</label>
                    <input type="text" class="form-control @error('nama_santri') is-invalid @enderror" id="nama_santri" name='nama_santri' value="{{$datasantri->nama_santri}}" placeholder="Masukkan Judul">
                    @error('nama_santri')<div class="invalid-feedback">{{$message}}</div>@enderror
                  </div>
                  <!--input nomor jenis kelamin-->
                  <fieldset class="form-group">
                    <div class="row">
                      <legend class="col-form-label col-sm-2 pt-0 ">Jenis Kelamin</legend>
                      <div class="col-sm-10">
                        <div class="form-check">
                          <input class="form-check-input " type="radio" name="jenis_kelamin" id="jk_laki" value="1" @if( $datasantri->jenis_kelamin == '1') checked @endif >
                          <label class="form-check-label" for="jk_laki">
                            Laki-Laki
                          </label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="jenis_kelamin" id="jk_perempuan" value="0" @if( $datasantri->jenis_kelamin == '0') checked @endif >
                          <label class="form-check-label" for="jk_perempuan">
                            Perempuan
                          </label>
                        </div>
                      </div>
                    </div>
                  </fieldset>
                  <!-- asal domisili-->
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="Provinsi">Pilih Provinsi</label>
                        <select class="form-control" id="Provinsi" name='Provinsi'>
                            <option value="Jawa Timur" @if( $datasantri->Provinsi == 'Jawa Timur') selected @endif>JAWA TIMUR</option>
                            <option value="Aceh" @if( $datasantri->Provinsi == 'Aceh') selected @endif> ACEH </option>
                            <option value="Sumatera Utara" @if( $datasantri->Provinsi == 'Sumatera Utara') selected @endif>SUMATERA UTARA</option>
                            <option value="Sumatra Barat" @if( $datasantri->Provinsi == 'Sumatera Barat') selected @endif>SUMATERA BARAT</option>
                            <option value="Riau" @if( $datasantri->Provinsi == 'Riau') selected @endif> KEPULAUAN RIAU</option>
                            <option value="Jambi" @if( $datasantri->Provinsi == 'Jambi') selected @endif>JAMBI</option>
                            <option value="Sumatera Selatan" @if( $datasantri->Provinsi == 'Sumatera Selatan') selected @endif>SUMATERA SELATAN</option>
                            <option value="Bengkulu" @if( $datasantri->Provinsi == 'Bengkulu') selected @endif>BENGKULU</option>
                            <option value="Lampung" @if( $datasantri->Provinsi == 'Lampung') selected @endif>LAMPUNG</option>
                            <option value="Bangka Belitung" @if( $datasantri->Provinsi == 'Bangka Belitung') selected @endif>KEPULAUAN BANGKA BELITUNG</option>
                            <option value="Jakarta" @if( $datasantri->Provinsi == 'Jakarta') selected @endif>DKI JAKARTA</option>
                            <option value="Jawa Barat" @if( $datasantri->Provinsi == 'Jawa Barat') selected @endif>JAWA BARAT</option>
                            <option value="Jawa Tengah" @if( $datasantri->Provinsi == 'Jawa Tengah') selected @endif>JAWA TENGAH</option>
                            <option value="Yogyakarta" @if( $datasantri->Provinsi == 'Yogyakarta') selected @endif>DI YOGYAKARTA</option>
                            <option value="Banten" @if( $datasantri->Provinsi == 'Banten') selected @endif>BANTEN</option>
                            <option value="Bali" @if( $datasantri->Provinsi == 'Bali') selected @endif>BALI</option>
                            <option value="Nusa Tenggara Barat" @if( $datasantri->Provinsi == 'Nusa Tenggara Barat') selected @endif>NUSA TENGGARA BARAT</option>
                            <option value="Nusa Tenggara Timur" @if( $datasantri->Provinsi == 'Nusa Tenggara Timur') selected @endif>NUSA TENGGARA TIMUR</option>
                            <option value="Kalimantan Barat" @if( $datasantri->Provinsi == 'Kalimantan Barat') selected @endif>KALIMANTAN BARAT</option>
                            <option value="Kalimantan Tengah" @if( $datasantri->Provinsi == 'Kalimantan Tengah') selected @endif>KALIMANTAN TENGAH</option>
                            <option value="Kalimantan Selatan" @if( $datasantri->Provinsi == 'Kalimantan Selatan') selected @endif>KALIMANTAN SELATAN</option>
                            <option value="Kalimantan Timur" @if( $datasantri->Provinsi == 'Kalimantan Timur') selected @endif>KALIMANTAN TIMUR</option>
                            <option value="Kalimantan Utara" @if( $datasantri->Provinsi == 'Kalimantan Utara') selected @endif>KALIMANTAN UTARA</option>
                            <option value="Sulawesi Utara" @if( $datasantri->Provinsi == 'Sulawesi Utara') selected @endif>SULAWESI UTARA</option>
                            <option value="Sulawesi Selatan" @if( $datasantri->Provinsi == 'Sulawesi Selatan') selected @endif>SULAWESI SELATAN</option>
                            <option value="Sulawesi Tengah" @if( $datasantri->Provinsi == 'Sulawesi Tengah') selected @endif>SULAWESI TENGAH</option>
                            <option value="Sulawesi Tenggara" @if( $datasantri->Provinsi == 'Sulawesi Tenggara') selected @endif>SULAWESI TENGGARA</option>
                            <option value="Gorontalo" @if( $datasantri->Provinsi == 'Gorontalo') selected @endif>GORONTALO</option>
                            <option value="Sulawesi Barat" @if( $datasantri->Provinsi == 'Sulawesi Barat') selected @endif>SULAWESI BARAT </option>
                            <option value="Maluku" @if( $datasantri->Provinsi == 'Maluku') selected @endif>MALUKU</option>
                            <option value="Maluku Utara" @if( $datasantri->Provinsi == 'Maluku Utara') selected @endif>MALUKU UTARA</option>
                            <option value="Papua" @if( $datasantri->Provinsi == 'Papua') selected @endif>PAPUA</option>
                            <option value="Papua Barat" @if( $datasantri->Provinsi == 'Papua Barat') selected @endif>PAPUA BARAT</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label for="Kota">Kota</label>
                      <input type="text" class="form-control @error('Kota') is-invalid @enderror" id="Kota" name='Kota' value="{{$datasantri->Kota}} " placeholder="tempat">
                      @error('Kota')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                  </div>
                  <!--alamat-->
                  <div class="form-group">
                      <label for="Alamat">Alamat Lengkap</label>
                      <textarea class="form-control @error('Alamat') is-invalid @enderror" id="Alamat" name="Alamat" rows="2"  placeholder="Enter ...">{{$datasantri->Alamat}}</textarea>
                      @error('Alamat')<div class="invalid-feedback">{{$message}}</div>@enderror
                  </div>
                  <!--input Asrama-->
                  <div class="form-group">
                    <label for="asrama">Pilih Asrama</label>
                    <select class="form-control" id="asrama" name='asrama'>
                      <option value='Al-Jadid' @if( $datasantri->asrama == 'Al-Jadid') selected @endif>Al-Jadid</option>
                      <option value='Al-Hikam' @if( $datasantri->asrama == 'Al-Hikam') selected @endif>Al-Hikam</option>
                      <option value='Al-Taufiq' @if( $datasantri->asrama == 'Al-Taufiq') selected @endif>Al-Taufiq</option>
                      <option value='Al-Huda' @if( $datasantri->asrama == 'Al-Huda') selected @endif>Al-Huda</option>
                      <option value='At-Tarbiyah' @if( $datasantri->asrama == 'At-Tarbiyah') selected @endif>At-Tarbiyah</option>
                      <option value="As-Syafaah" @if( $datasantri->asrama == 'As-Syafaah') selected @endif>As-Syafa'ah</option>
                      <option value='An-Nadroh' @if( $datasantri->asrama == 'An-Nadroh') selected @endif>An-Nadroh</option>
                      <option value='Al-Karomah' @if( $datasantri->asrama == 'Al-Karomah') selected @endif>Al-Karomah</option>
                      <option value='Al-Hidayah' @if( $datasantri->asrama == 'Al-Hidayah') selected @endif>Al-Hidayah</option>
                      <option value="Al-Ma'rifah" @if( $datasantri->asrama == "Al-Ma'rifah") selected @endif>Al-Ma'rifah</option>
                      <option value='Istiqomah' @if( $datasantri->asrama == 'Istiqomah') selected @endif>Istiqomah</option>
                    </select>
                  </div>
                  <!--input sekolah-->
                  <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="pendidikan">Pilih Pendidikan</label>
                    <select class="form-control" id="pendidikan" name='tingkat_pendidikan'>
                      <option value="SD" @if( $datasantri->tingkat_pendidikan == 'SD') selected @endif>SD</option>
                      <option value="SMP" @if( $datasantri->tingkat_pendidikan == 'SMP') selected @endif>SMP</option>
                      <option value="SMA" @if( $datasantri->tingkat_pendidikan == 'SMA') selected @endif>SMA</option>
                      <option value="Universitas" @if( $datasantri->tingkat_pendidikan == 'Universitas') selected @endif>Universitas</option>
                      <option value="Hanya Pondok" @if( $datasantri->tingkat_pendidikan == 'Hanya Pondok') selected @endif>Hanya Pondok</option>
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="pendidikan">Pilih Pendidikan Diniyah</label>
                    <select class="form-control" id="pendidikan" name='tingkat_diniyah'>
                      <option value="MI" @if( $datasantri->tingkat_diniyah == 'MI') selected @endif>MI</option>
                      <option value="MTs" @if( $datasantri->tingkat_diniyah == 'MTs') selected @endif>MTs</option>
                      <option value="MA" @if( $datasantri->tingkat_diniyah == 'MA') selected @endif>MA</option>
                    </select>
                  </div>
                </div>
                  <!--lahiran-->
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="tanggal_lahir">Tahun lahir</label>
                      <input type="date" class="form-control" id="tanggal_lahir" name='tanggal_lahir' value={{$datasantri->tanggal_lahir}}>
                    </div>
                    <div class="form-group col-md-6">
                      <label for="tempat_lahir">Tempat Lahir</label>
                      <input type="text" class="form-control @error('tempat_lahir') is-invalid @enderror" id="tempat_lahir" name='tempat_lahir' value="{{$datasantri->tempat_lahir}} "placeholder="tempat lahir">
                      @error('tempat_lahir')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                  </div>
                  <!--nama wali-->
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="nama_wali">Nama Wali</label>
                      <input type="text" class="form-control @error('nama_wali') is-invalid @enderror" id="nama_wali" value='{{$datasantri->nama_wali}}' name='nama_wali' >
                      @error('nama_wali')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                    <div class="form-group col-md-6">
                      <label for="no_hp">No. Hp</label>
                      <input type="number" class="form-control @error('no_hp') is-invalid @enderror" id="no_hp" name='no_hp' value="{{$datasantri->no_hp}}" placeholder="isi nomor wali">
                      @error('no_hp')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                  </div>
                  
                  <!--file input-->
                  <div class="form-group">
                    <label for="exampleFormControlFile1">Upload Foto</label>
                    <input type="file" name='foto_santri' class="form-control-file" id="exampleFormControlFile1">
                  </div>
                  {{-- input tgl daftar --}}
                  <div class="form-group col-md-6">
                    <label for="tanggal_daftar">Tanggal Pendaftaran</label>
                    <input type="date" class="form-control" id="tanggal_daftar" name='tanggal_daftar' value={{$datasantri->tanggal_daftar}}  >
                  </div>
                  <!--input nomor jenis kelamin-->
                  @if(auth()->user()->role == 'superadmin')
                  <fieldset class="form-group">
                    <div class="row">
                      <legend class="col-form-label col-sm-2 pt-0">Status Santri</legend>
                      <div class="col-sm-10">
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="status" id="status_aktif" value="1" @if( $datasantri->status == '1') checked @endif>
                          <label class="form-check-label" for="status_aktif">
                            Aktif
                          </label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="status" id="status_tidak" value="0" @if( $datasantri->status == '0') checked @endif>
                          <label class="form-check-label" for="status_tidak">
                            Tidak Aktif
                          </label>
                        </div>
                      </div>
                    </div>
                  </fieldset>
                  @endif
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-warning">Ubah</button>
                  <a type="button" class="btn btn-primary" href="/santri">Kembali</a>
                </div>
              </form>
            </div>
@endsection
