@extends('adminlte.master')

@section('content')

<div class="card card-warning">
              <div class="card-header">
                <h3 class="card-title">Edit Surat Izin Pergi</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action='/pulang/{{$pulang->id_pulang}}/update' method='POST' enctype="multipart/form-data">
                @method('PATCH')
                    @csrf
                <div class="card-body">
                    <!-- input Nama Santri-->
                    <div class="form-group">
                        <label for="id_santri">Nama Santri</label>
                        <select class="selectpicker form-control " id="id_santri" name='id_santri' data-live-search="true">
                            @foreach($datasantri as $dts)
                            @if($dts->status == '1')
                            <option  data-tokens="{{$dts->nama_santri}}"  value="{{$dts->id_santri}}" @if( $dts->id_santri == $pulang->id_santri) selected @endif>{{$dts->nama_santri}}</option>
                            @endif
                            @endforeach
                        </select>
                        @error('id_santri')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                    <!-- input Nomor Surat -->
                  <div class="form-group">
                    <label for="nomor_surat">Nomor Surat</label>
                    <input type="text" class="form-control @error('nomor_surat') is-invalid @enderror" id="nomor_surat" name='nomor_surat' value="{{$pulang->nomor_surat}}" placeholder="Masukkan nomor surat">
                    @error('nomor_surat')<div class="invalid-feedback">{{$message}}</div>@enderror
                  </div>
                  <!-- input tujuan -->
                  <div class="form-group">
                    <label for="tujuan">Tujuan</label>
                    <input type="text" class="form-control @error('tujuan') is-invalid @enderror" id="tujuan" name='tujuan' value="{{$pulang->tujuan}}" placeholder="Masukkan tujuan">
                    @error('tujuan')<div class="invalid-feedback">{{$message}}</div>@enderror
                  </div>
                  <!-- input keperluan -->
                  <div class="form-group">
                    <label for="keperluan">Keperluan</label>
                    <input type="text" class="form-control @error('keperluan') is-invalid @enderror" id="keperluan" name='keperluan' value="{{$pulang->keperluan}}" placeholder="Masukkan Judul">
                    @error('keperluan')<div class="invalid-feedback">{{$message}}</div>@enderror
                  </div>
                  {{-- input waktu --}}
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="tanggal_pergi">Tanggal Pergi</label>
                      <input type="date" class="form-control @error('tanggal_pergi') is-invalid @enderror" id="tanggal_pergi" name='tanggal_pergi' value="{{$pulang->tanggal_pergi}}" placeholder="tempat">
                      @error('tanggal_pergi')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                    <div class="form-group col-md-6">
                      <label for="tanggal_balik">Tanggal Balik</label>
                      <input type="date" class="form-control @error('tanggal_balik') is-invalid @enderror" id="tanggal_balik" name='tanggal_balik' value="{{$pulang->tanggal_balik}}" placeholder="tempat">
                      @error('tanggal_balik')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                  </div>

                  <!--input Status-->
                  <fieldset class="form-group">
                    <div class="row">
                      <legend class="col-form-label col-sm-4 pt-0">Status Santri</legend>
                      <div class="col-sm-8">
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="status" id="status_balik" value="1" @if( $pulang->status == '1') checked @endif>
                          <label class="form-check-label" for="status_balik">
                            Sudah Balik
                          </label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="status" id="status_belum" value="0" @if( $pulang->status == '0') checked @endif>
                          <label class="form-check-label" for="status_belum">
                            Belum Balik
                          </label>
                        </div>
                      </div>
                    </div>
                  </fieldset>
                  <div class="form-group">
                    <input type="hidden" class="form-control" id="user_id" name='user_id' value="{{auth()->user()->id}}"  >
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-warning" onclick="return confirm('data akan diupdate...')">Ubah</button>
                  <a type="button" class="btn btn-primary" href="/pergi">Kembali</a>
                </div>
              </form>
            </div>
@endsection
