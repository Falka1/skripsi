<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        #judul{
            text-align:center;
            line-height: 0.2;
        }
        #isi{ margin: 1cm,2cm,0cm;}
        body{
            line-height: 1.2;
        }
       

    </style>
</head>
<body>
    <img src="{{public_path('/images/kop/kop1.jpg')}}"  width="100%">
    <div id=isi>
        <p id=judul style="font-size: 14pt;"><b><u>SURAT IZIN PULANG</u></b></p>
        <p id=judul style="font-size: 12pt; text-transform: uppercase;">Nomor: {{$pulang->nomor_surat}}</p>
        <p>Dengan ini kami memberikan izin seperlunya kepada,</p>
        <table style="border-spacing: 2px; cellpadding=0;">
            <tr>
                <td style="width: 30%; font-size: 12pt;">Nama</td>
                <td style="width: 5%; font-size: 12pt;">:   </td>
                <td style="width: 65%; font-size: 12pt; text-transform: capitalize;">{{$pulang->datasantri->nama_santri}} </td>
            </tr>
            <tr>
                <td style="width: 30%; font-size: 12pt;">Alamat asal</td>
                <td style="width: 5%; font-size: 12pt;" >:   </td>
                <td style="width: 65%; font-size: 12pt; text-transform: capitalize;">{{$pulang->tujuan}}</td>
            </tr>
            <tr>
                <td style="width: 30%; vertical-align: top; font-size: 12pt;">Tanggal</td>
                <td style="width: 5%; vertical-align: top; font-size: 12pt;">:</td>
                <td style="width: 65%; font-size: 12pt;"><?php echo date("h:i",strtotime($pulang->waktu_pergi));?> s.d <?php echo date("h:i",strtotime($pulang->waktu_balik));?></td>
            </tr>
            <tr>
                <td style="width: 30%; vertical-align: top; font-size: 12pt;">Keperluan</td>
                <td style="width: 5%; vertical-align: top; font-size: 12pt;">:</td>
                <td style="width: 65%; font-size: 12pt; text-transform: capitalize;">{{$pulang->keperluan}}</td>
            </tr>
        </table>
        <p  style="font-size: 12pt;">Demikian surat izin ini kami berikan, agar dipergunakan sebagaimana mestinya dan setelah sampai dipondok dikembalikan kepada Pengurus dengan membayar administrasi Rp. 5000,-.</p>
    </div>
    <?php
        $pecahkan = explode(' ', $pulang->created_at);
        $date = $pecahkan[0];
            function tgl_indo($tanggal){
                $bulan = array (
                    1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                );
                $pecahkan = explode('-', $tanggal);
                
                // variabel pecahkan 0 = tanggal
                // variabel pecahkan 1 = bulan
                // variabel pecahkan 2 = tahun
             
                return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
            }?>
    <div style="justify-content:space-between ">
        <div style="width: 30%; text-align: center; float: left; font-size: 12pt;">Mengetahui</div>
        <div style="width: 30%; text-align: center; float: right; font-size: 12pt;">Kediri, <?php echo  tgl_indo($date);?></div>
    </div><br>
    <div style="justify-content:space-between ">
        <div style="width: 30%; text-align: center; float: left; font-size: 12pt;">Ketua Pondok</div>
        <div style="width: 30%; text-align: center; float: right; font-size: 12pt;">Seksi Keamanan</div>
    </div>
    <br><br><br><br>
    <div style="justify-content:space-between ">
        <div style="width: 30%; text-align: center; float: left; font-size: 12pt; text-transform: uppercase;"><b><u>AFIF AFANDI,S.E.</u></b></div>
        <div style="width: 30%; text-align: center; float: right; font-size: 12pt; text-transform: uppercase;"><b><u>{{$pulang->User->name}}</u></b></div>
    </div>
    <div style=" text-align: center; font-size: 12pt;">Menyetujui / Acc<br>Pengasuh Pondok</div><br><br><br>
    <div style=" text-align: center; font-size: 12pt;"><b><u>K.ABDUL MAJID ALI FIKRI, RA.</u></b></div>
</body>
</html>