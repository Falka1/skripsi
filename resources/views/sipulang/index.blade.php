@extends('adminlte.master')
@section('content')
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Surat Izin Pulang</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item active">SI Pulang</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->

      <div class="card card-outline card-olive">
      <div class="card-header">
        <h3 class="card-title">TABEL SURAT IZIN PULANG</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        
        @if (session('status'))
          <div class="alert alert-success">
              {{session('status')}}
          </div>
        @endif

        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#TambahModal">
          Tambah Surat
        </button>
        <div class="table-responsive">
        <table id="datasantri" class="table table-hover table-bordered table-striped">
          <thead>
          <tr>
            <th>No</th>
            <th>Nama Santri</th>
            <th>Tujuan</th>
            <th>Keterangan</th>
            <th>Waktu Pergi</th>
            <th>Waktu Balik</th>
            <th>Status</th>
            <th>Petugas</th>
            <th style="width:20%">Action</th>
          </tr>
          </thead>
          <tbody>
            @forelse($pulang as $pg)
            <tr>
              <th>{{$loop->iteration}}</th>
              <td><a  href="/santri/{{$pg->id_santri}}/show"> {{$pg->datasantri->nama_santri}}</a></td>
              <td>{{$pg->tujuan}}</td>
              <td>{{$pg->keperluan}}</td>
              <td>{{$pg->tanggal_pergi}}</td>
              <td>{{$pg->tanggal_balik}}</td>
              @if ($pg->status == 1)
                  <td><span class="badge badge-success">Sudah Balik</span></td>
              @else 
                <td><span class="badge badge-danger">Belum Balik</span></td>
              @endif
              <td> {{$pg->User->name}}</td>
              <td class='row'>
                <a type="button" class="btn btn-success" href="/pulang/{{$pg->id_pulang}}/pdf" target="_blank"><i class="fas fa-print"></i></a>
                <a type="button" class="btn btn-warning" href="/pulang/{{$pg->id_pulang}}/edit"><i class="fas fa-edit"></i></a>
                @if(auth()->user()->role == 'superadmin')
                <form action="/pulang/{{$pg->id_pulang}}" method="post">
                  @csrf
                  @method('DELETE')
                <button type="submit" class="btn btn-danger" value='delete' onclick="return confirm('data akan dihapus...')"><i class="far fa-trash-alt"></i></button>
                </form>
                @endif
              </td>
            </tr>
            @empty
              <tr>
                <td colspan="4" align="center">data masih kosong</td>
              </tr>
            @endforelse
          </tbody>
          <tfoot>
          <tr>
            <th>No</th>
            <th>Nama Santri</th>
            <th>Tujuan</th>
            <th>Keterangan</th>
            <th>Waktu Pergi</th>
            <th>Waktu Balik</th>
            <th>Status</th>
            <th>Petugas</th>
            <th style="width:20%">Action</th>
          </tr>
          </tfoot>
        </table>
        </div>
      </div>
      <!-- /.card-body -->
    </div>
      <!-- /.card -->
    </section>

    <!-- Modal -->
    <div class="modal fade" id="TambahModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Tambah Surat Izin Pulang</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <!-- form start -->
            <form role="form" action='/pulang' method='POST' enctype="multipart/form-data">
              @csrf
                <!-- input Nama Santri-->
                    <div class="form-group">
                        <label for="id_santri">Nama Santri</label>
                        <select class="selectpicker form-control " id="id_santri" name='id_santri' data-live-search="true">
                            @foreach($datasantri as $dts)
                            @if($dts->status == '1')
                            <option  data-tokens="{{$dts->nama_santri}}"  value="{{$dts->id_santri}}">{{$dts->nama_santri}}</option>
                            @endif
                            @endforeach
                        </select>
                        @error('id_santri')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                    <!-- input Nomor Surat -->
                  <div class="form-group">
                    <label for="nomor_surat">Nomor Surat</label>
                    <input type="text" class="form-control @error('nomor_surat') is-invalid @enderror" id="nomor_surat" name='nomor_surat' value="{{old('nomor_surat','')}} " placeholder="Masukkan nomor surat">
                    @error('nomor_surat')<div class="invalid-feedback">{{$message}}</div>@enderror
                  </div>
                  <!-- input tujuan -->
                  <div class="form-group">
                    <label for="tujuan">Tujuan</label>
                    <input type="text" class="form-control @error('tujuan') is-invalid @enderror" id="tujuan" name='tujuan' value="{{old('tujuan','')}} " placeholder="Masukkan tujuan">
                    @error('tujuan')<div class="invalid-feedback">{{$message}}</div>@enderror
                  </div>
                  <!-- input keperluan -->
                  <div class="form-group">
                    <label for="keperluan">Keperluan</label>
                    <input type="text" class="form-control @error('keperluan') is-invalid @enderror" id="keperluan" name='keperluan' value="{{old('keperluan','')}} " placeholder="Masukkan Judul">
                    @error('keperluan')<div class="invalid-feedback">{{$message}}</div>@enderror
                  </div>
                  {{-- input waktu --}}
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="tanggal_pergi">Tanggal Pergi</label>
                      <input type="date" class="form-control @error('tanggal_pergi') is-invalid @enderror" id="tanggal_pergi" name='tanggal_pergi' value="{{old('tanggal_pergi','')}} " placeholder="tempat">
                      @error('tanggal_pergi')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                    <div class="form-group col-md-6">
                      <label for="tanggal_balik">Tanggal Balik</label>
                      <input type="date" class="form-control @error('tanggal_balik') is-invalid @enderror" id="tanggal_balik" name='tanggal_balik' value="{{old('tanggal_balik','')}} " placeholder="tempat">
                      @error('tanggal_balik')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                  </div>

                  <!--input Status-->
                  <fieldset class="form-group">
                    <div class="row">
                      <legend class="col-form-label col-sm-4 pt-0">Status Santri</legend>
                      <div class="col-sm-8">
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="status" id="status_balik" value="1" >
                          <label class="form-check-label" for="status_balik">
                            Sudah Balik
                          </label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="status" id="status_belum" value="0" checked>
                          <label class="form-check-label" for="status_belum">
                            Belum Balik
                          </label>
                        </div>
                      </div>
                    </div>
                  </fieldset>
                  <div class="form-group">
                    <input type="hidden" class="form-control" id="user_id" name='user_id' value="{{auth()->user()->id}}"  >
                  </div>
               
              
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-success">Simpan</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
          </form>
          </div>
        </div>
      </div>
    </div>
    
@endsection

@push('script')
<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/jszip/jszip.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>
<script>
  $(function () {
    $("#datasantri").DataTable({
  "responsive": true, "lengthChange": false, "autoWidth": false,
  "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
}).buttons().container().appendTo('#datasantri_wrapper .col-md-6:eq(0)');
  });
  
</script>
  
@endpush

