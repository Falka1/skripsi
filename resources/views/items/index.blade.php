@extends('adminlte.master')
@section('content')

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Selamat Datang</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Welcome</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">


        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Santri Yang belum balik</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table id="datasantri" class="table table-hover table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Santri</th>
                  <th>Tujuan</th>
                  <th>Keterangan</th>
                  <th>Waktu Pergi</th>
                  <th>Waktu Balik</th>
                  <th>Status</th>
                  <th>Petugas</th>
                </tr>
                </thead>
                <tbody>
                  @forelse($pergi as $pg)
                  <tr>
                    <th>{{$loop->iteration}}</th>
                    <td><a  href="/santri/{{$pg->id_santri}}/show"> {{$pg->datasantri->nama_santri}}</a></td>
                    <td>{{$pg->tujuan}}</td>
                    <td>{{$pg->keterangan}}</td>
                    <td>{{$pg->waktu_pergi}}</td>
                    <td>{{$pg->waktu_balik}}</td>
                    @if ($pg->status == 0)
                      <td><span class="badge badge-danger">Belum Balik</span></td>
                    @endif
                    <td> {{$pg->User->name}}</td>
                  </tr>
                  @empty
                    <tr>
                      <td colspan="8" align="center">data kosong</td>
                    </tr>
                  @endforelse
                </tbody>
              </table>
              </div>
          </div>
          <div class="card-footer">
            <a type="button" class="btn btn-primary" href="/pergi">Menuju Tabel</a>
          </div>
        </div>
  
    <div class="row">
      <div class="col-sm-6">
        <div class="card card-olive">
          <div class="card-header">
            <h3 class="card-title">Grafik Surat Izin Pergi</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove">
                <i class="fas fa-times"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
            <div class="chart">
              <canvas id="barChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <div class="col-sm-6">
        <div class="card card-olive">
          <div class="card-header">
            <h3 class="card-title">Grafik Pelanggaran Santri</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="chart">
              <canvas id="barChart2" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
            </div>
          </div>
        </div>
      </div>
    </div>

    </section>
    <!-- /.content -->

@endsection

@push('script')

<script src="{{asset('adminlte/plugins/chart.js/Chart.min.js')}}"></script>

<script>
  $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */


    var areaChartData = {
      labels  : [
        <?php $count = 0; ?>
        @foreach ($pergi1 as $prg)
          <?php if($count == 7) break; ?>
          ' {{ $prg->Tanggal}}',
           <?php $count++; ?>
        @endforeach
      ],
      datasets: [
        {
          label               : 'Santri yang izin pergi',
          backgroundColor     : 'rgba(60,141,188,0.9)',
          borderColor         : 'rgba(60,141,188,0.8)',
          pointRadius          : false,
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : [
            <?php $count = 0; ?>
            @foreach ($pergi1 as $prg)
              <?php if($count == 7) break; ?>
              ' {{ $prg->jumlah}}',
              <?php $count++; ?>
            @endforeach
          ]
        },
      ]
    }
console.log(areaChartData.label)
  

    
    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $('#barChart').get(0).getContext('2d')
    var barChartData = $.extend(true, {}, areaChartData)

    console.log(barChartData)
    var barChartOptions = {
      responsive              : true,
      maintainAspectRatio     : false,
      datasetFill             : false,

    scales: {
        yAxes: [{
            ticks: {
                beginAtZero: true
            }
        }]
    }

    }

    new Chart(barChartCanvas, {
      type: 'bar',
      data: barChartData,
      options: barChartOptions
      
    })


    var langgarChart = {
      labels  : [
        <?php $count = 0; ?>
        @foreach ($langgar as $lgr)
          <?php if($count == 12) break; ?>
          ' {{ $lgr->Bulan}}',
           <?php $count++; ?>
        @endforeach
      ],
      datasets: [
        {
          label               : 'Santri yang melanggar',
          backgroundColor     : 'rgba(60,141,188,0.9)',
          borderColor         : 'rgba(60,141,188,0.8)',
          pointRadius          : false,
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : [
            <?php $count = 0; ?>
            @foreach ($langgar as $lgr)
              <?php if($count == 12) break; ?>
              ' {{ $lgr->jumlah}}',
              <?php $count++; ?>
            @endforeach
          ]
        },
      ]
    }
console.log(langgarChart.label)
  

    
    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $('#barChart2').get(0).getContext('2d')
    var barChartData = $.extend(true, {}, langgarChart)

    console.log(barChartData)
    var barChartOptions = {
      responsive              : true,
      maintainAspectRatio     : false,
      datasetFill             : false,
      
    scales: {
        yAxes: [{
            ticks: {
                beginAtZero: true
            }
        }]
    }
    }

    new Chart(barChartCanvas, {
      type: 'bar',
      data: barChartData,
      options: barChartOptions
    })
  })
</script>
  
@endpush