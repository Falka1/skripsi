@extends('adminlte.master')
@section('content')
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Admin</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Admin</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->

      <div class="card card-outline card-olive">
      <div class="card-header">
        <h3 class="card-title">TABEL DATA ADMIN</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        
        @if (session('status'))
          <div class="alert alert-success">
              {{session('status')}}
          </div>
        @endif

        <a class="btn btn-primary mb-2" href="/admin/create">Tambah Admin</a>
        <div class="table-responsive">
        <table id="datasantri" class="table table-hover table-bordered table-striped">
          <thead>
          <tr>
            <th>No</th>
            <th>Nama Admin</th>
            <th>Email</th>
            <th>Role</th>
            <th style="width:20%">Action</th>
          </tr>
          </thead>
          <tbody>
            @forelse($admin as $adm)
            <tr>
              <th>{{$loop->iteration}}</th>
              <td>{{$adm->name}}</td>
              <td>{{$adm->email}}</td>
              <td> {{$adm->role}}</td>
              <td class='row'>
                <a type="button" class="btn btn-success" href="/admin/{{$adm->id }}/edit"><i class="fas fa-edit"></i></a>
                <form action="/admin/{{$adm->id }}" method="post">
                  @csrf
                  @method('DELETE')
                <button type="submit" class="btn btn-danger" value='delete' onclick="return confirm('data akan dihapus...')"><i class="far fa-trash-alt"></i></button>
                </form>
              </td>
            </tr>
            @empty
              <tr>
                <td colspan="4" align="center">data masih kosong</td>
              </tr>
            @endforelse
          </tbody>
          <tfoot>
          <tr>
            <th>No</th>
            <th>Nama Admin</th>
            <th>Email</th>
            <th>Role</th>
            <th style="width:20%">Action</th>
          </tr>
          </tfoot>
        </table>
        </div>
      </div>
      <!-- /.card-body -->
    </div>
      <!-- /.card -->
      

    </section>
    
@endsection

@push('script')
<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/jszip/jszip.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>
<script>
  $(function () {
    $("#datasantri").DataTable({
  "responsive": true, "lengthChange": false, "autoWidth": false,
  "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
}).buttons().container().appendTo('#datasantri_wrapper .col-md-6:eq(0)');
  });
  
</script>
@endpush

