@extends('adminlte.master')

@section('content')

<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create Admin</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action='/admin' method='POST' enctype="multipart/form-data">
              @csrf
                <div class="card-body">
                  <!-- input nomor nama -->
                  <div class="form-group">
                    <label for="name">Nama Admin</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name='name' value="{{old('name','')}} " placeholder="Masukkan nama">
                    @error('name')<div class="invalid-feedback">{{$message}}</div>@enderror
                  </div>
                  {{-- password --}}
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name='email' value="{{old('email','')}} " placeholder="Masukkan email">
                    @error('email')<div class="invalid-feedback">{{$message}}</div>@enderror
                  </div>
                  {{-- password --}}
                  <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name='password'  placeholder="Masukkan password">
                    @error('password')<div class="invalid-feedback">{{$message}}</div>@enderror
                  </div>
                  
                  
                  <!--role-->
                  <div class="form-group">
                    <label for="role">Pilih Role</label>
                    <select class="form-control" id="role" name='role'>
                      <option value="admin">Admin</option>
                      <option value="superadmin">Super Admin</option>
                    </select>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Simpan</button>
                  <a type="button" class="btn btn-primary" href="/santri">Kembali</a>
                </div>
              </form>
            </div>
@endsection
