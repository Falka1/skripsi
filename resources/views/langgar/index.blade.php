@extends('adminlte.master')
@section('content')
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Surat Pernyataan Pelanggaran</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item active">SP</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->

      <div class="card card-outline card-olive">
      <div class="card-header">
        <h3 class="card-title">TABEL SANTRI LANGGAR</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        
        @if (session('status'))
          <div class="alert alert-success">
              {{session('status')}}
          </div>
        @endif

        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#TambahModal">
          Tambah Surat
        </button>
        <div class="table-responsive">
        <table id="datasantri" class="table table-hover table-bordered table-striped">
          <thead>
          <tr>
            <th>No</th>
            <th>Nama Santri</th>
            <th>Tanggal Kejadian</th>
            <th>Keterangan</th>
            <th>Petugas</th>
            <th style="width:20%">Action</th>
          </tr>
          </thead>
          <tbody>
            @forelse($langgar as $lgr)
            <tr>
              <th>{{$loop->iteration}}</th>
              <td><a  href="/santri/{{$lgr->id_santri}}/show"> {{$lgr->datasantri->nama_santri}}</a></td>
              <td>{{$lgr->tanggal_langgar}}</td>
              <td>{{$lgr->keterangan}}</td>
              <td> {{$lgr->User->name}}</td>
              <td class='row'>
                <a type="button" class="btn btn-success" href="/langgar/{{$lgr->id_langgar}}/pdf" target="_blank"><i class="fas fa-print"></i></a>
                <a type="button" class="btn btn-warning" href="/langgar/{{$lgr->id_langgar}}/edit"><i class="fas fa-edit"></i></a>
                @if(auth()->user()->role == 'superadmin')
                <form action="/langgar/{{$lgr->id_langgar}}" method="post">
                  @csrf
                  @method('DELETE')
                <button type="submit" class="btn btn-danger" value='delete' onclick="return confirm('data akan dihapus...')"><i class="far fa-trash-alt"></i></button>
                </form>
                @endif
              </td>
            </tr>
            @empty
              <tr>
                <td colspan="4" align="center">data masih kosong</td>
              </tr>
            @endforelse
          </tbody>
          <tfoot>
          <tr>
            <th>No</th>
            <th>Nama Santri</th>
            <th>Tanggal Kejadian</th>
            <th>Keterangan</th>
            <th>Petugas</th>
            <th style="width:20%">Action</th>
          </tr>
          </tr>
          </tfoot>
        </table>
        </div>
      </div>
      <!-- /.card-body -->
    </div>
      <!-- /.card -->
    </section>

    <!-- Modal -->
    <div class="modal fade" id="TambahModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Tambah Surat Pelanggaran</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <!-- form start -->
            <form role="form" action='/langgar' method='POST' enctype="multipart/form-data">
              @csrf
                <!-- input Nama Santri-->
                    <div class="form-group">
                        <label for="id_santri">Nama Santri</label>
                        <select class="selectpicker form-control " id="id_santri" name='id_santri' data-live-search="true">
                            @foreach($datasantri as $dts)
                            @if($dts->status == '1')
                            <option  data-tokens="{{$dts->nama_santri}}"  value="{{$dts->id_santri}}">{{$dts->nama_santri}}</option>
                            @endif
                            @endforeach
                        </select>
                        @error('id_santri')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                  <!-- input tanggal_langgar -->
                  <div class="form-group">
                    <label for="tanggal_langgar">Tanggal Pelanggaran</label>
                    <input type="date" class="form-control" id="tanggal_langgar" name='tanggal_langgar' value={{old('tanggal_langgar','')}} >
                    @error('tanggal_langgar')<div class="invalid-feedback">{{$message}}</div>@enderror
                  </div>
                  <!-- input keterangan -->
                  <div class="form-group">
                    <label for="keterangan">Keterangan</label>
                    <textarea class="form-control @error('Alamat') is-invalid @enderror" id="keterangan" name="keterangan" rows="2"  placeholder="Enter ...">{{old('keterangan','')}}</textarea>
                    @error('keterangan')<div class="invalid-feedback">{{$message}}</div>@enderror
                  </div>
                

                  <div class="form-group">
                    <input type="hidden" class="form-control" id="user_id" name='user_id' value="{{auth()->user()->id}}"  >
                  </div>
               
              
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-success">Simpan</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
          </form>
          </div>
        </div>
      </div>
    </div>
    
@endsection

@push('script')
<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/jszip/jszip.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>
<script>
  $(function () {
    $("#datasantri").DataTable({
  "responsive": true, "lengthChange": false, "autoWidth": false,
  "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
}).buttons().container().appendTo('#datasantri_wrapper .col-md-6:eq(0)');
  });
  
</script>
  
@endpush

