@extends('adminlte.master')

@section('content')

<div class="card card-warning">
              <div class="card-header">
                <h3 class="card-title">Edit Surat Pelanggaran</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action='/langgar/{{$langgar->id_langgar}}/update' method='POST' enctype="multipart/form-data">
                @method('PATCH')
                @csrf
                <div class="card-body">
                    <!-- input Nama Santri-->
                    <div class="form-group {{$errors->has('id_santri') ? 'has-error': ''}}" >
                        <label for="id_santri">Nama Santri</label>
                        <select class="selectpicker form-control " id="id_santri" name='id_santri' data-live-search="true" @error('id_santri') is-invalid @enderror>
                            @foreach($datasantri as $dts)
                            @if($dts->status == '1')
                            <option  data-tokens="{{$dts->nama_santri}}"  value="{{$dts->id_santri}}" @if( $dts->id_santri == $langgar->id_santri) selected @endif>{{$dts->nama_santri}}</option>
                            @endif
                            @endforeach
                        </select>
                        @if($errors->has('id_santri'))
                        <div class="has-block">{{$errors->first('id_santri')}}</div>
                        @endif
                    </div>
                    
                  
                  
                  {{-- input waktu --}}
                    <div class="form-group">
                      <label for="tanggal_langgar">Tanggal Pelanggaran</label>
                      <input type="date" class="form-control @error('tanggal_langgar') is-invalid @enderror" id="tanggal_langgar" name='tanggal_langgar' value="{{$langgar->tanggal_langgar}}" placeholder="tempat">
                      @error('tanggal_langgar')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>

                    <!-- input keterangan -->
                  <div class="form-group">
                    <label for="keterangan">Keterangan</label>
                    <textarea class="form-control @error('Alamat') is-invalid @enderror" id="keterangan" name="keterangan" rows="2"  placeholder="Enter ...">{{$langgar->keterangan}}</textarea>
                    @error('keterangan')<div class="invalid-feedback">{{$message}}</div>@enderror
                  </div>
                    
                  <div class="form-group">
                    <input type="hidden" class="form-control" id="user_id" name='user_id' value="{{auth()->user()->id}}"  >
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-warning" onclick="return confirm('data akan diupdate...')">Ubah</button>
                  <a type="button" class="btn btn-primary" href="/langgar">Kembali</a>
                </div>
              </form>
            </div>
@endsection
