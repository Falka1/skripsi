<!DOCTYPE html>
<head>
    <title>Surat izin Pergi</title>
    <meta charset="utf-8">
    <link href="https://repo.rachmat.id/jquery-ui-1.12.1/jquery-ui.css" rel="stylesheet">
    <script type="text/javascript" src="https://repo.rachmat.id/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="https://repo.rachmat.id/jquery-ui-1.12.1/jquery-ui.js"></script>
    <script type="text/javascript">
        $(function(){
        $("#datepicker").datepicker({
            dateFormat:"dd-mm-yy",
        });
        });
    </script>

    <style>
        #judul{
            text-align:center;
        }
       

    </style>

</head>

<body>
    <div>
        <h4 id=judul><u>SURAT PERNYATAAN PELANGGARAN</u></h4>
        

        <p style="font-size: 12px;">Yang bertanda tangan dibawah ini saya :</p>

        <table>
            <tr>
                <td style="width: 30%; font-size: 12px;">Nama</td>
                <td style="width: 5%; font-size: 12px;">:   </td>
                <td style="width: 65%; font-size: 12px;">{{$langgar->datasantri->nama_santri}} </td>
            </tr>
            <tr>
                <td style="width: 30%; font-size: 12px;">Alamat</td>
                <td style="width: 5%; font-size: 12px;" >:   </td>
                <td style="width: 65%; font-size: 12px;">{{$langgar->datasantri->Alamat}}</td>
            </tr>
            <tr>
                <td style="width: 30%; vertical-align: top; font-size: 12px;">Kamar</td>
                <td style="width: 5%; vertical-align: top; font-size: 12px;">:</td>
                <td style="width: 65%; font-size: 12px;">{{$langgar->datasantri->asrama}}</td>
            </tr>
            <tr>
                <td style="width: 30%; vertical-align: top; font-size: 12px;">Kelas Pagi/Malam</td>
                <td style="width: 5%; vertical-align: top; font-size: 12px;">:</td>
                <td style="width: 65%; font-size: 12px;">{{$langgar->datasantri->tingkat_pendidikan}}, {{$langgar->datasantri->tingkat_diniyah}}</td>
            </tr>
            <tr>
                <td style="width: 30%; font-size: 12px;">Nama Wali</td>
                <td style="width: 5%; font-size: 12px;">:</td>
                <td style="width: 65%; font-size: 12px;">{{$langgar->datasantri->nama_wali}} </td>
            </tr>
            <tr>
                <td style="width: 30%; font-size: 12px;">Nomor Telepon</td>
                <td style="width: 5%; font-size: 12px;">:</td>
                <td style="width: 65%; font-size: 12px;">{{$langgar->datasantri->no_hp}} </td>
            </tr>
            <?php
            function tgl_indo($tanggal){
                $bulan = array (
                    1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                );
                $pecahkan1 = explode(' ', $tanggal);
                $pecahkan = explode('-', $pecahkan1[0]);
                
                // variabel pecahkan 0 = tanggal
                // variabel pecahkan 1 = bulan
                // variabel pecahkan 2 = tahun
             
                return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
            }?>
            <tr>
                <td style="width: 30%; font-size: 12px;">Tanggal Pelanggaran</td>
                <td style="width: 5%; font-size: 12px;">:</td>
                <td style="width: 65%; font-size: 12px;"><?php echo tgl_indo($langgar->tanggal_langgar);?></td>
            </tr>
        </table>

        <p  style="font-size: 12px;">Dengan ini menyatakan bahwa saya telah melakukan pelanggaran,adapun jenis pelanggaran sebagai berikut: <b style="text-transform: uppercase;">{{$langgar->keterangan}} </b></p>
        
        
        <div style="width: 30%; text-align: center; float: right; font-size: 12px;">Kediri, <?php echo tgl_indo($langgar->created_at);?></div><br>
        <div  class="flex-container" style="justify-content:space-between ">
            <div style="width: 30%; text-align: center; float: left; font-size: 12px;">Keamanan Pondok</div>
            <div style="width: 100%; text-align: center; float: center; font-size: 12px;">Orang Tua</div>
            <div style="width: 30%; text-align: center; float: right; font-size: 12px;">Santri</div>
        </div>
        <br><br><br><br>
        <div style="justify-content:space-between ">
            <div style="width: 30%; text-align: center; float: left; font-size: 12px; text-transform: uppercase;"><u>{{$langgar->User->name}}</u></div>
            <div style="width: 100%; text-align: center; float: center; font-size: 12px; text-transform: uppercase;"><u>{{$langgar->datasantri->nama_wali}}</u></div>
            <div style="width: 30%; text-align: center; float: right; font-size: 12px; text-transform: uppercase;"><u>{{$langgar->datasantri->nama_santri}}</u></div>
        </div><br><br>
        <div style="width: 100%; text-align: center; float: center; font-size: 12px;">Mengetahui Pramu Pondok</div><br><br><br><br>
        <div style="width: 100%; text-align: center; float: center; font-size: 12px; text-transform: uppercase;"><u>AFIF AFANDI,S.E.</u></div>
       
    </div>
</body>

</html>