<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
      <img src="{{asset('adminlte/dist/img/AdminLTELogo.png')}}"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">KEDUNGLO</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="/admin" class="d-block">{{auth()->user()->name}} </a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="/" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="/santri" class="nav-link">
              <i class="nav-icon fas fa-user-friends"></i>
              <p>
                Data Santri
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Tabel Surat
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/pergi" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Surat Izin Pergi</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/pulang" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Surat Izin Pulang</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/boyong" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Surat Boyong</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/langgar" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Surat Pelanggaran</p>
                </a>
              </li>
            </ul>
          </li>
          @if(auth()->user()->role == 'superadmin')
          <li class="nav-item has-treeview">
            <a href="/admin" class="nav-link">
              <i class="nav-icon fas fa-user-cog"></i>
              <p>
                Pengaturan Admin
              </p>
            </a>
          </li>
          @endif
         
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>