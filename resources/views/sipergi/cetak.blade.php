<!DOCTYPE html>
<head>
    <title>Surat izin Pergi</title>
    <meta charset="utf-8">
    <link href="https://repo.rachmat.id/jquery-ui-1.12.1/jquery-ui.css" rel="stylesheet">
    <script type="text/javascript" src="https://repo.rachmat.id/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="https://repo.rachmat.id/jquery-ui-1.12.1/jquery-ui.js"></script>
    <script type="text/javascript">
        $(function(){
        $("#datepicker").datepicker({
            dateFormat:"dd-mm-yy",
        });
        });
    </script>

    <style>
        #judul{
            text-align:center;
        }
       

    </style>

</head>

<body>
    <div>
        <h4 id=judul style="font-size: 18px;">PON-PES KEDUNGLO<br> AL MUNADHDHOROH</h4>
        <h5 id=judul><u>SURAT IZIN PERGI</u></h5>
        

        <p style="font-size: 12px;">Dengan ini kami memberikan izin kepada :</p>

        <table>
            <tr>
                <td style="width: 30%; font-size: 12px;">Nama</td>
                <td style="width: 5%; font-size: 12px;">:   </td>
                <td style="width: 65%; font-size: 12px;">{{$pergi->datasantri->nama_santri}} </td>
            </tr>
            <tr>
                <td style="width: 30%; font-size: 12px;">Kamar</td>
                <td style="width: 5%; font-size: 12px;" >:   </td>
                <td style="width: 65%; font-size: 12px;">{{$pergi->datasantri->asrama}}</td>
            </tr>
            <tr>
                <td style="width: 30%; vertical-align: top; font-size: 12px;">Tujuan</td>
                <td style="width: 5%; vertical-align: top; font-size: 12px;">:</td>
                <td style="width: 65%; font-size: 12px;">{{$pergi->tujuan}}</td>
            </tr>
            <tr>
                <td style="width: 30%; vertical-align: top; font-size: 12px;">Keperluan</td>
                <td style="width: 5%; vertical-align: top; font-size: 12px;">:</td>
                <td style="width: 65%; font-size: 12px;">{{$pergi->keterangan}}</td>
            </tr>
            <tr>
                <td style="width: 30%; font-size: 12px;">Jam</td>
                <td style="width: 5%; font-size: 12px;">:</td>
                <td style="width: 65%; font-size: 12px;"><?php echo date("h:i",strtotime($pergi->waktu_balik));?></td>
            </tr>
        </table>

        <p  style="font-size: 12px;">Demikian surat izin ini kami berikan, agar dipergunakan sebagaimana mestinya dan setelah sampai dipondok dikembalikan kepada Pengurus dengan membayar administrasi Rp. 500,-.</p>
        <?php
            function tgl_indo($tanggal){
                $bulan = array (
                    1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                );
                $pecahkan1 = explode(' ', $tanggal);
                $pecahkan = explode('-', $pecahkan1[0]);
                
                // variabel pecahkan 0 = tanggal
                // variabel pecahkan 1 = bulan
                // variabel pecahkan 2 = tahun
             
                return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
            }?>
        <div style="width: 40%; text-align: center; float: right; font-size: 12px;">Kediri, <?php echo tgl_indo($pergi->created_at);?></div><br>
        <div style="justify-content:space-between ">
            <div style="width: 40%; text-align: center; float: left; font-size: 12px;">Santri</div>
            <div style="width: 40%; text-align: center; float: right; font-size: 12px;">Seksi Keamanan</div>
        </div>
        <br><br><br><br>
        <div style="justify-content:space-between ">
            <div style="width: 40%; text-align: center; float: left; font-size: 12px; text-transform: uppercase;"><u>{{$pergi->datasantri->nama_santri}}</u></div>
            <div style="width: 40%; text-align: center; float: right; font-size: 12px; text-transform: uppercase;"><u>{{$pergi->User->name}}<u/></div>
        </div>
       
    </div>
</body>

</html>