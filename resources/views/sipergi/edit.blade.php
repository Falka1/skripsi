@extends('adminlte.master')

@section('content')

<div class="card card-warning">
              <div class="card-header">
                <h3 class="card-title">Edit Surat Izin Pergi</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action='/pergi/{{$pergi->id_pergi}}/update' method='POST' enctype="multipart/form-data">
                @method('PATCH')
                @csrf
                <div class="card-body">
                  
                    <!-- input Nama Santri-->
                        <div class="form-group">
                            <label for="id_santri">Nama Santri</label>
                            <select class="selectpicker form-control " id="id_santri" name='id_santri' data-live-search="true">
                                @foreach($datasantri as $dts)
                                @if($dts->status == '1')
                                <option  data-tokens="{{$dts->nama_santri}}"  value="{{$dts->id_santri}}" @if( $dts->id_santri == $pergi->id_santri) selected @endif>{{$dts->nama_santri}}</option>
                                @endif
                                @endforeach
                            </select>
                            @error('id_santri')<div class="invalid-feedback">{{$message}}</div>@enderror
                        </div>
                      <!-- input tujuan -->
                      <div class="form-group">
                        <label for="tujuan">Tujuan</label>
                        <input type="text" class="form-control @error('tujuan') is-invalid @enderror" id="tujuan" name='tujuan' value="{{$pergi->tujuan}} " placeholder="Masukkan tujuan">
                        @error('tujuan')<div class="invalid-feedback">{{$message}}</div>@enderror
                      </div>
                      <!-- input keterangan -->
                      <div class="form-group">
                        <label for="keterangan">Keterangan</label>
                        <input type="text" class="form-control @error('keterangan') is-invalid @enderror" id="keterangan" name='keterangan' value="{{$pergi->keterangan}} " placeholder="Masukkan Judul">
                        @error('keterangan')<div class="invalid-feedback">{{$message}}</div>@enderror
                      </div>
                      {{-- input waktu --}}
                      <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="waktu_pergi">Waktu Pergi</label>
                          <input type="time" class="form-control @error('waktu_pergi') is-invalid @enderror" id="waktu_pergi" name='waktu_pergi' value="{{$pergi->waktu_pergi}}" placeholder="tempat">
                        </div>
                        <div class="form-group col-md-6">
                          <label for="waktu_balik">Waktu Balik</label>
                          <input type="time" class="form-control @error('waktu_balik') is-invalid @enderror" id="waktu_balik" name='waktu_balik' value="{{$pergi->waktu_balik}}" placeholder="tempat">
                          @error('waktu_balik')<div class="invalid-feedback">{{$message}}</div>@enderror
                        </div>
                      </div>
    
                      <!--input Status-->
                      <fieldset class="form-group">
                        <div class="row">
                          <legend class="col-form-label col-sm-4 pt-0">Status Santri</legend>
                          <div class="col-sm-8">
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="status" id="status_balik" value="1" @if( $pergi->status == '1') checked @endif>
                              <label class="form-check-label" for="status_balik">
                                Sudah Balik
                              </label>
                            </div>
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="status" id="status_belum" value="0" @if( $pergi->status == '0') checked @endif>
                              <label class="form-check-label" for="status_belum">
                                Belum Balik
                              </label>
                            </div>
                          </div>
                        </div>
                      </fieldset>
                      <div class="form-group">
                        <input type="hidden" class="form-control" id="user_id" name='user_id' value="{{auth()->user()->id}}"  >
                      </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-warning">Ubah</button>
                  <a type="button" class="btn btn-primary" href="/pergi">Kembali</a>
                </div>
              </form>
            </div>
@endsection
