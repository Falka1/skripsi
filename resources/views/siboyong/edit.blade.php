@extends('adminlte.master')

@section('content')

<div class="card card-warning">
              <div class="card-header">
                <h3 class="card-title">Edit Surat Izin Pergi</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action='/boyong/{{$boyong->id_boyong}}/update' method='POST' enctype="multipart/form-data">
                @method('PATCH')
                @csrf
                <div class="card-body">
                    <!-- input Nama Santri-->
                    <div class="form-group {{$errors->has('id_santri') ? 'has-error': ''}}" >
                        <label for="id_santri">Nama Santri</label>
                        <select class="selectpicker form-control " id="id_santri" name='id_santri' data-live-search="true" @error('id_santri') is-invalid @enderror>
                            @foreach($datasantri as $dts)
                            @if($dts->status == '1')
                            <option  data-tokens="{{$dts->nama_santri}}"  value="{{$dts->id_santri}}" @if( $dts->id_santri == $boyong->id_santri) selected @endif>{{$dts->nama_santri}}</option>
                            @endif
                            @endforeach
                        </select>
                        @if($errors->has('id_santri'))
                        <div class="has-block">{{$errors->first('id_santri')}}</div>
                        @endif
                    </div>
                    <!-- input Nomor Surat -->
                  <div class="form-group">
                    <label for="nomor_surat">Nomor</label>
                    <input type="text" class="form-control @error('nomor_surat') is-invalid @enderror" id="nomor_surat" name='nomor_surat' value="{{$boyong->nomor_surat}} " placeholder="Masukkan nomor surat">
                    @error('nomor_surat')<div class="invalid-feedback">{{$message}}</div>@enderror
                  </div>
                  
                  
                  {{-- input waktu --}}
                    <div class="form-group">
                      <label for="tanggal_boyong">Tanggal Boyong</label>
                      <input type="date" class="form-control @error('tanggal_boyong') is-invalid @enderror" id="tanggal_boyong" name='tanggal_boyong' value="{{$boyong->tanggal_boyong}}" placeholder="tempat">
                      @error('tanggal_boyong')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                    

                  <!--input Status-->
                  <fieldset class="form-group">
                    <div class="row">
                      <legend class="col-form-label col-sm-4 pt-0">Status Santri</legend>
                      <div class="col-sm-8">
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="status" id="status_balik" value="1" @if( $boyong->status == '1') checked @endif>
                          <label class="form-check-label" for="status_balik">
                            Boyong
                          </label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="status" id="status_belum" value="0" @if( $boyong->status == '0') checked @endif>
                          <label class="form-check-label" for="status_belum">
                            Diboyongkan
                          </label>
                        </div>
                      </div>
                    </div>
                  </fieldset>
                  <div class="form-group">
                    <input type="hidden" class="form-control" id="user_id" name='user_id' value="{{auth()->user()->id}}"  >
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-warning" onclick="return confirm('data akan diupdate...')">Ubah</button>
                  <a type="button" class="btn btn-primary" href="/boyong">Kembali</a>
                </div>
              </form>
            </div>
@endsection
