<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        #judul{
            text-align:center;
            line-height: 0.8;
        }
        #isi{ margin: 1cm;}
       

    </style>
</head>
<body>
    <img src="{{public_path('/images/kop/kop1.jpg')}}"  width="100%">
    <div id=isi>
    <p id=judul style="font-size: 16pt;"><b><i>FAFIRRUU ILALLOH</i></b></p>
    <p id=judul style="font-size: 16pt;"><b><u>SURAT KETERANGAN PINDAH SANTRI (BOYONG)</u></b></p>
    <p id=judul style="font-size: 12pt;">Nomor: {{$boyong->nomor_surat}}</p><br><br>

    <p style="font-size: 12pt;">Surat Keterangan ini diberikan kepada :</p>
    <table style="border-spacing: 10px;">
        <tr>
            <td style="width: 30%; font-size: 12pt;">Nomor Induk</td>
            <td style="width: 5%; font-size: 12pt;">:   </td>
            <td style="width: 65%; font-size: 12pt;">{{$boyong->datasantri->nomor_induk}} </td>
        </tr>
        <tr>
            <td style="width: 30%; font-size: 12pt;">Nama</td>
            <td style="width: 5%; font-size: 12pt;">:   </td>
            <td style="width: 65%; font-size: 12pt;">{{$boyong->datasantri->nama_santri}} </td>
        </tr>
        <tr>
            <td style="width: 30%; font-size: 12pt;">Tempat tanggal lahir</td>
            <td style="width: 5%; font-size: 12pt;" >:   </td>
            <td style="width: 65%; font-size: 12pt;">{{$boyong->datasantri->tempat_lahir}}, {{$boyong->datasantri->tanggal_lahir}}</td>
        </tr>
        
        <tr>
            <td style="width: 30%; vertical-align: top; font-size: 12pt;">Jenis Kelamin</td>
            <td style="width: 5%; vertical-align: top; font-size: 12pt;">:</td>
            @if ($boyong->datasantri->jenis_kelamin == 1)
            <td style="width: 65%; font-size: 12pt;">Laki-laki</td>
            @else 
            <td style="width: 65%; font-size: 12pt;">Perempuan</td>
            @endif
            
        </tr>
        <tr>
            <td style="width: 30%; vertical-align: top; font-size: 12pt;">Orang tua</td>
            <td style="width: 5%; vertical-align: top; font-size: 12pt;">:</td>
            <td style="width: 65%; font-size: 12pt;">{{$boyong->datasantri->nama_wali}}</td>
        </tr>
        <tr>
            <td style="width: 30%; vertical-align: top; font-size: 12pt;">Alamat asal</td>
            <td style="width: 5%; vertical-align: top; font-size: 12pt;">:</td>
            <td style="width: 65%; font-size: 12pt;">{{$boyong->datasantri->Alamat}}, {{$boyong->datasantri->Kota}}, {{$boyong->datasantri->Provinsi}}</td>
        </tr>
        <tr>
            <td style="width: 30%; vertical-align: top; font-size: 12pt;">Pendidikan Diniyah</td>
            <td style="width: 5%; vertical-align: top; font-size: 12pt;">:</td>
            @if ($boyong->datasantri->tingkat_diniyah == 'MI')
            <td style="width: 65%; font-size: 12pt;">Madrasah Ibtidaiyah</td>
            @elseif($boyong->datasantri->tingkat_diniyah == 'MTs') 
            <td style="width: 65%; font-size: 12pt;">Madrasah Tsanawiyah</td>
            @else
            <td style="width: 65%; font-size: 12pt;">Madrasah Aliyah</td>
            @endif
        </tr>
        <tr>
            <td style="width: 30%; vertical-align: top; font-size: 12pt;">Pendidikan Umum</td>
            <td style="width: 5%; vertical-align: top; font-size: 12pt;">:</td>
            @if ($boyong->datasantri->tingkat_pendidikan == 'Hanya Pondok')
            <td style="width: 65%; font-size: 12pt;"></td>\
            @else
            <td style="width: 65%; font-size: 12pt;">{{$boyong->datasantri->tingkat_pendidikan}} Wahidiyah Kota Kediri</td>
            @endif
        </tr>
        <tr>
            <td style="width: 30%; vertical-align: top; font-size: 12pt;">Terdaftar tahun</td>
            <td style="width: 5%; vertical-align: top; font-size: 12pt;">:</td>
            <td style="width: 65%; font-size: 12pt;"><?php echo date("Y",strtotime($boyong->datasantri->tanggal_daftar));?></td>
        </tr>
    </table>
    <?php
        $pecahkan = explode(' ', $boyong->created_at);
        $date = $pecahkan[0];
            function tgl_indo($tanggal){
                $bulan = array (
                    1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                );
                $pecahkan1 = explode(' ', $tanggal);
                $pecahkan = explode('-', $pecahkan1[0]);
                // variabel pecahkan 0 = tanggal
                // variabel pecahkan 1 = bulan
                // variabel pecahkan 2 = tahun
             
                return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
            }?>
    <p style="font-size: 12pt; line-height: 1.5;">Menerangkan bahwa santri tersebut diatas selama di Pondok Pesantren Kedunglo Al-Munadhoroh berkelakuan <?php echo ($boyong->status == 1) ? 'baik' : 'tidak baik' ; ?>
    dan mulai tanggal <b><?php echo tgl_indo($boyong->created_at);?></b> sudah tidak menjadi santri di Pondok Pesantren Kedunglo Al-Munadhdhoroh Kota Kediri <?php echo ($boyong->status == 1) ? '(BOYONG)' : '(DIBOYONGKAN)' ; ?>
    </p>
    <p>Demikian surat keterangan ini kami buat, semoga ilmu yang diperoleh selama di Pondok Pesantren bener-bener bermanfa'at fiddini waddunya wal akhiroh. Amiin. </p>
    </div>
    <div style="width: 40%; text-align: center; float: right; font-size: 12pt;">
        <table style="justify-content:space-between; text-align: justify;">
            <tr>
                <td style="width: 50%; vertical-align: top; font-size: 12pt;">Kedunglo,</td> 
                <td style="width: 50%; font-size: 12pt;"><u><?php use \go2hi\go2hi;echo go2hi::date('d F Y', go2hi::GO2HI_HIJRI, $date);  ?>H</u><br> <?php echo  tgl_indo($date);?>M</td>
                
            </tr>
        </table>
    </div><br><br><br>
    <div style="width: 50%; text-align: center; float: right; font-size: 12pt;">PONDOK PESANTREN PUTRA-PUTRI KEDUNGLO AL-MUNADHDHOROH</div><br><br><br>
        <div style="justify-content:space-between ">
            <div style="width: 30%; text-align: center; float: left; font-size: 12pt;">Pemegang</div>
            <div style="width: 50%; text-align: center; float: right; font-size: 12pt;">Pengasuh,</div>
        </div>
        <br><br><br><br><br><br>
        <div style="justify-content:space-between ">
            <div style="width: 30%; text-align: center; float: left; font-size: 12pt; text-transform: uppercase;"><b><u>{{$boyong->datasantri->nama_santri}}</u></b></div>
            <div style="width: 50%; text-align: center; float: right; font-size: 12pt;"><b><u>KANJENG KYAI ABDUL MAJID ALI FIKRI, RA.</u></b></div>
        </div>
        
</body>
</html>