@extends('adminlte.master')
@section('content')
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Boyong</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Boyong</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->

      <div class="card card-outline card-olive">
      <div class="card-header">
        <h3 class="card-title">TABEL SURAT BOYONG</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        
        @if (session('status'))
          <div class="alert alert-success">
              {{session('status')}}
          </div>
        @endif

        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#TambahModal">
          Tambah Surat
        </button>
        <div class="table-responsive">
        <table id="datasantri" class="table table-hover table-bordered table-striped">
          <thead>
          <tr>
            <th>No</th>
            <th>Nama Santri</th>
            <th>Nomor Surat</th>
            <th>Tanggal Boyong</th>
            <th>Status</th>
            <th>Admin</th>
            <th style="width:20%">Action</th>
          </tr>
          </thead>
          <tbody>
            @forelse($boyong as $byg)
            <tr>
              <th>{{$loop->iteration}}</th>
              <td><a  href="/santri/{{$byg->id_santri}}/show"> {{$byg->datasantri->nama_santri}}</a></td>
              <td>{{$byg->nomor_surat}}</td>
              <td>{{$byg->tanggal_boyong}}</td>
              @if ($byg->status == 1)
                  <td><span class="badge badge-success">Boyong</span></td>
              @else 
                <td><span class="badge badge-danger">Diboyongkan</span></td>
              @endif
              <td> {{$byg->User->name}}</td>
              <td class='row'>
                <a type="button" class="btn btn-success" href="/boyong/{{$byg->id_boyong}}/pdf" target="_blank"><i class="fas fa-print"></i></a>
                <a type="button" class="btn btn-warning" href="/boyong/{{$byg->id_boyong}}/edit"><i class="fas fa-edit"></i></a>
                @if(auth()->user()->role == 'superadmin')
                <form action="/boyong/{{$byg->id_boyong}}" method="post">
                  @csrf
                  @method('DELETE')
                <button type="submit" class="btn btn-danger" value='delete' onclick="return confirm('data akan dihapus...')"><i class="far fa-trash-alt"></i></button>
                </form>
                @endif
              </td>
            </tr>
            @empty
              <tr>
                <td colspan="4" align="center">data masih kosong</td>
              </tr>
            @endforelse
          </tbody>
          <tfoot>
          <tr>
            <th>No</th>
            <th>Nama Santri</th>
            <th>Nomor Surat</th>
            <th>Tanggal Boyong</th>
            <th>Status</th>
            <th>Admin</th>
            <th style="width:20%">Action</th>
          </tr>
          </tfoot>
        </table>
        </div>
      </div>
      <!-- /.card-body -->
    </div>
      <!-- /.card -->
    </section>

    <!-- Modal -->
    <div class="modal fade" id="TambahModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Tambah Surat Boyong</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <!-- form start -->
            <form role="form" action='/boyong' method='POST' enctype="multipart/form-data">
              @csrf
                <!-- input Nama Santri-->
                    <div class="form-group {{$errors->has('id_santri') ? 'has-error': ''}}" >
                        <label for="id_santri">Nama Santri</label>
                        <select class="selectpicker form-control " id="id_santri" name='id_santri' data-live-search="true" @error('id_santri') is-invalid @enderror>
                            @foreach($datasantri as $dts)
                            @if($dts->status == '1')
                            <option  data-tokens="{{$dts->nama_santri}}"  value="{{$dts->id_santri}}">{{$dts->nama_santri}}</option>
                            @endif
                            @endforeach
                        </select>
                        @if($errors->has('id_santri'))
                        <div class="has-block">{{$errors->first('id_santri')}}</div>
                        @endif
                    </div>
                    <!-- input Nomor Surat -->
                  <div class="form-group">
                    <label for="nomor_surat">Nomor</label>
                    <input type="text" class="form-control @error('nomor_surat') is-invalid @enderror" id="nomor_surat" name='nomor_surat' value="{{old('nomor_surat','')}} " placeholder="Masukkan nomor surat">
                    @error('nomor_surat')<div class="invalid-feedback">{{$message}}</div>@enderror
                  </div>
                  
                  
                  {{-- input waktu --}}
                    <div class="form-group">
                      <label for="tanggal_boyong">Tanggal Boyong</label>
                      <input type="date" class="form-control @error('tanggal_boyong') is-invalid @enderror" id="tanggal_boyong" name='tanggal_boyong' value="{{old('tanggal_boyong','')}} " placeholder="tempat">
                      @error('tanggal_boyong')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                    

                  <!--input Status-->
                  <fieldset class="form-group">
                    <div class="row">
                      <legend class="col-form-label col-sm-4 pt-0">Status Santri</legend>
                      <div class="col-sm-8">
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="status" id="status_balik" value="1" >
                          <label class="form-check-label" for="status_balik">
                            Boyong
                          </label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="status" id="status_belum" value="0" checked>
                          <label class="form-check-label" for="status_belum">
                            Diboyongkan
                          </label>
                        </div>
                      </div>
                    </div>
                  </fieldset>
                  <div class="form-group">
                    <input type="hidden" class="form-control" id="user_id" name='user_id' value="{{auth()->user()->id}}"  >
                  </div>
               
              
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-success">Simpan</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
          </form>
          </div>
        </div>
      </div>
    </div>
    
@endsection

@push('script')
<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/jszip/jszip.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>
<script>
  $(function () {
    $("#datasantri").DataTable({
  "responsive": true, "lengthChange": false, "autoWidth": false,
  "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
}).buttons().container().appendTo('#datasantri_wrapper .col-md-6:eq(0)');
  });
  
</script>
  
@endpush

