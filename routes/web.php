<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\SantriController;
use Illuminate\Routing\RouteGroup;

Route::get('/master', function () {
    return view('adminlte.master');
});
Route::get('/cetak', function () {
    return view('siboyong.cetak');
});


Route::get('/admin', function () {
    return view('admin.create');
});

//route login
Route::get('/login', 'AuthController@login')->name('login');
Route::post('/postlogin', 'AuthController@postlogin');
Route::get('/logout', 'AuthController@logout');


Route::group(['middleware' =>  ['auth', 'CheckRole:superadmin,admin']], function () {
    //route dashboard
    Route::get('/', 'DashboardController@index');
    Route::get('/contact', 'DashboardController@contact');
    //route untuk data santri
    Route::get('/santri', 'SantriController@index');
    Route::get('/santri/create', 'SantriController@create');
    Route::post('/santri', 'SantriController@store');
    Route::delete('/santri/{id}', 'SantriController@destroy');
    Route::get('/santri/{id}/show', 'SantriController@show');
    Route::get('/santri/{id}/edit', 'SantriController@edit');
    Route::patch('/santri/{id}/update', 'SantriController@update');
    //route pergi
    Route::get('/pergi', 'PergiController@index');
    Route::post('/pergi', 'PergiController@store');
    Route::get('/pergi/{id}/edit', 'PergiController@edit');
    Route::patch('/pergi/{id}/update', 'PergiController@update');
    Route::delete('/pergi/{id}', 'PergiController@destroy');
    Route::get('/pergi/{id}/pdf', 'PergiController@exportpdf');
    //route pulang
    Route::get('/pulang', 'PulangController@index');
    Route::post('/pulang', 'PulangController@store');
    Route::get('/pulang/{id}/edit', 'PulangController@edit');
    Route::patch('/pulang/{id}/update', 'PulangController@update');
    Route::delete('/pulang/{id}', 'PulangController@destroy');
    Route::get('/pulang/{id}/pdf', 'PulangController@exportpdf');
    //route boyong
    Route::get('/boyong', 'BoyongController@index');
    Route::post('/boyong', 'BoyongController@store');
    Route::delete('/boyong/{id}', 'BoyongController@destroy');
    Route::get('/boyong/{id}/edit', 'BoyongController@edit');
    Route::patch('/boyong/{id}/update', 'BoyongController@update');
    Route::get('/boyong/{id}/pdf', 'BoyongController@exportpdf');
    //langgar
    Route::get('/langgar', 'LanggarController@index');
    Route::post('/langgar', 'LanggarController@store');
    Route::delete('/langgar/{id}', 'LanggarController@destroy');
    Route::get('/langgar/{id}/edit', 'LanggarController@edit');
    Route::patch('/langgar/{id}/update', 'LanggarController@update');
    Route::get('/langgar/{id}/pdf', 'LanggarController@exportpdf');
});

Route::group(['middleware' => ['auth', 'CheckRole:superadmin']], function () {
    //route admin
    Route::get('/admin', 'AdminController@index');
    Route::get('/admin/create', 'AdminController@create');
    Route::post('/admin', 'AdminController@store');
    Route::delete('/admin/{id}', 'AdminController@destroy');
    Route::get('/admin/{id}/edit', 'AdminController@edit');
    Route::post('/admin/{id}/update', 'AdminController@update');
});
