<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataSantriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_santri', function (Blueprint $table) {
            $table->bigIncrements('id_santri');
            $table->string('nomor_induk')->unique();
            $table->string('nama_santri');
            $table->boolean('jenis_kelamin');
            $table->string('asrama');
            $table->string('tingkat_pendidikan');
            $table->string('tingkat_diniyah');
            $table->date('tanggal_lahir');
            $table->string('tempat_lahir');
            $table->string('nama_wali');
            $table->string('no_hp');
            $table->string('Provinsi');
            $table->string('Kota');
            $table->string('Alamat');
            $table->string('foto_santri');
            $table->date('tanggal_daftar');
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_santri');
    }
}
