<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSipergiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sipergi', function (Blueprint $table) {
            $table->bigIncrements('id_pergi');
            $table->string('tujuan');
            $table->string('keterangan');
            $table->time('waktu_pergi');
            $table->time('waktu_balik');
            $table->unsignedBigInteger('id_santri');
            $table->foreign('id_santri')->references('id_santri')->on('data_santri');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sipergi');
    }
}
