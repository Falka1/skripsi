<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLanggarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('langgar', function (Blueprint $table) {
            $table->bigIncrements('id_langgar');
            $table->date('tanggal_langgar');
            $table->string('keterangan');
            $table->unsignedBigInteger('id_santri');
            $table->foreign('id_santri')->references('id_santri')->on('data_santri');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('langgar');
    }
}
