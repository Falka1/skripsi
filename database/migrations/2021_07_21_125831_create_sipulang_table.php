<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSipulangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sipulang', function (Blueprint $table) {
            $table->bigIncrements('id_pulang');
            $table->string('nomor_surat');
            $table->string('tujuan');
            $table->string('keperluan');
            $table->date('tanggal_pergi');
            $table->date('tanggal_balik');
            $table->unsignedBigInteger('id_santri');
            $table->foreign('id_santri')->references('id_santri')->on('data_santri');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sipulang');
    }
}
